defmodule Webapp.Teams do
  @moduledoc """
  The Teams context.
  """

import Ecto.Query, warn: false
alias Webapp.Repo

alias Webapp.Notifications.Notifications
alias Webapp.Teams.Team
alias Webapp.Teams.Member
alias Webapp.Teams.Invitation
alias Webapp.Accounts
alias Webapp.Accounts.User


  @doc """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Team{}, ...]

  """
  def list_teams(preloads \\ []) do
    Repo.all(Team)
    |> Repo.preload(preloads)
  end

  @doc """
  Returns the list of user's teams.
  """
  def list_user_teams(preloads \\ []) do
    Repo.all(Team)
    |> Repo.preload(preloads)
  end

  @doc """
  Gets a single team.

  Raises `Ecto.NoResultsError` if the Team does not exist.

  ## Examples

      iex> get_team!(123)
      %Team{}

      iex> get_team!(456)
      ** (Ecto.NoResultsError)

  """
  def get_team!(id, preloads \\ []) do
    Repo.get!(Team, id)
    |> Repo.preload(preloads)
  end

  @doc """
  Gets a team based on the params.
  """
  def get_team_by(conditions, preloads \\ [])

  def get_team_by(%{"namespace" => name}, preloads) do
    Repo.get_by(Team, namespace: name)
    |> Repo.preload(preloads)
  end

  @doc """
  Creates a team with first member.

  ## Examples

      iex> create_team(%{field: value})
      {:ok, %Team{}}

      iex> create_team(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_team(attrs \\ %{}) do
    result = %Team{}
    |> Team.add_team_changeset(attrs)
    |> Repo.insert()

    case result do
      {:ok, _team} ->
        Notifications.publish(:info, "Team #{attrs["name"]} created.")
      _ -> nil
    end

    result
  end

  @doc """
  Updates a team.

  ## Examples

      iex> update_team(team, %{field: new_value})
      {:ok, %Team{}}

      iex> update_team(team, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_team(%Team{} = team, attrs) do
    team
    |> Team.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Team.

  ## Examples

      iex> delete_team(team)
      {:ok, %Team{}}

      iex> delete_team(team)
      {:error, %Ecto.Changeset{}}

  """
  def delete_team(%Team{} = team) do
    Notifications.publish(:info, "Team #{team.name} deleted.")
    Repo.delete(team)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking team changes.

  ## Examples

      iex> change_team(team)
      %Ecto.Changeset{source: %Team{}}

  """
  def change_team(%Team{} = team) do
    Team.changeset(team, %{})
  end


  @doc """
  Check if the user is a member of the team.
  Returns true if the user is a member of the team or false if the user is not a member of the team.

  ## Examples

      iex> member_of_team?(user, team)
      true

      iex> member_of_team?(user, team)
      false
  """
  def member_of_team?(%User{} = user, %Team{} = team) do
    case get_user_team_member(user, team) do
      %Member{} -> true
      _ -> false
    end
  end

  @doc """
  Check if the user is a member of the team with a specific role.
  """
  def member_with_role?(%User{} = user, %Team{} = team, role) do
    case get_user_team_member(user, team) do
      %Member{role: member_role} ->
         member_role == role
      _ ->
        false
    end
  end


  @doc """
  Creates a member.

  ## Examples

      iex> create_member(%{field: value})
      {:ok, %Member{}}

      iex> create_member(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_member(attrs \\ %{}) do
    %Member{}
    |> Member.create_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a member.

  ## Examples

      iex> delete_member(member)
      {:ok, %Member{}}

      iex> delete_member(member)
      {:error, %Ecto.Changeset{}}
  """
  def delete_member(member) do
    Repo.delete(member)
  end


  @doc """
  Returns an `%Ecto.Changeset{}` for tracking member changes.
  """
  def change_member(%Member{} = member) do
    Member.update_changeset(member, %{})
  end

  @doc """
  Updates a member.

  ## Examples

      iex> update_member(member, %{field: new_value})
      {:ok, %Member{}}

      iex> update_member(member, %{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """
  def update_member(%Member{} = member, attrs) do
    member
    |> Member.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Get the user team member.

  ## Examples

      iex> get_user_team_member(user, team)
      %Member{}

      iex> get_user_team_member(user, team)
      nil
  """

  def get_user_team_member(%User{} = user, %Team{} = team) do
    Member
    |> where([m], m.user_id == ^user.id and m.team_id == ^team.id)
    |> Repo.one()
  end

  @doc """
  Gets or creates a user account and adds the user to the team.

  ## Examples

      iex> get_or_invite_user(email)
      {:ok, %User{}}

      iex> get_or_invite_user(email)
      {:error, %Ecto.Changeset{}}
  """
  def invite_member(email, %Team{} = team) do
    with {:ok, user} <- Accounts.get_or_invite_user(email),
         {:ok, _member} <- create_member(%{user_id: user.id, team_id: team.id}) do
      {:ok, user}
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        # Copy all errors from create_member changeset to the invite changeset
        new_changeset =
          Invitation.changeset(%Invitation{}, %{"email" => email})
          |> Map.put(:action, :insert)

        new_changeset = Enum.reduce(changeset.errors, new_changeset, fn {_field, {message, opts}}, acc ->
          Ecto.Changeset.add_error(acc, :email, message, opts)
        end)

        {:error, new_changeset}

      {:error, _error} ->
        {:error, "Cannot create a member profile"}
    end
  end
end
