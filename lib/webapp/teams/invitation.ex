defmodule Webapp.Teams.Invitation do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :email, :string
  end

  @doc false
  def changeset(invitation, attrs \\ %{}) do
    invitation
    |> cast(attrs, [:email])
    |> validate_required([:email])
  end
end
