defmodule Webapp.Teams.Member do
  use Ecto.Schema
  import Ecto.Changeset

  alias Webapp.{
    Types.UserRole,
    Accounts.User,
    Teams.Team,
    Types.UserRole
  }

  schema "members" do
    belongs_to(:user, User)
    belongs_to(:team, Team)
    field(:role, UserRole, default: "User")

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:role, :user_id])
    |> validate_required([:role, :user_id])
    |> assoc_constraint(:user)
    |> assoc_constraint(:team)
  end

  @doc false
  def create_changeset(team, attrs) do
    team
    |> cast(attrs, [:role, :user_id, :team_id])
    |> validate_required([:role, :user_id, :team_id])
    |> assoc_constraint(:user)
    |> assoc_constraint(:team)
    |> unique_constraint([:team_id, :user_id],
      message: "User is already a member of the team"
    )
  end

  def update_changeset(member, attrs) do
    member
    |> cast(attrs, [:role])
    |> validate_required([:role])
  end
end
