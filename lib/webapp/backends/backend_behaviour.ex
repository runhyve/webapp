defmodule Webapp.Hypervisors.BackendBehaviour do
  @moduledoc """
  This module defines the behaviour of a hypervisor backend.
  """

  alias Ecto.{Repo, Multi}
  alias Webapp.Machines.Machine
  alias Webapp.Hypervisors.Hypervisor
  alias Webapp.Networks.Network

  @type taskid() :: integer()

  @callback hypervisor_os_details(Hypervisor.t()) :: {:ok, map()} | {:error, String.t()}
  @callback hypervisor_status(Hypervisor.t()) :: {:ok, map()} | {:error, String.t()}

  @callback create_machine(Repo.t(), Multi.changes(), Machine.t()) :: {:ok, taskid()} | {:error, String.t()}
  @callback delete_machine(Machine.t()) :: {:ok, taskid()} | {:ok, nil} | {:error, String.t()}
  @callback delete_machine(Repo.t(), Multi.changes(), Machine.t()) :: {:ok, taskid()} | {:ok, nil} | {:error, String.t()}

  @callback start_machine(Machine.t()) :: {:ok, String.t()} | {:error, String.t()}
  @callback stop_machine(Machine.t()) :: {:ok, String.t()} | {:error, String.t()}
  @callback restart_machine(Machine.t()) :: {:ok, String.t()} | {:error, String.t()}
  @callback poweroff_machine(Machine.t()) :: {:ok, String.t()} | {:error, String.t()}
  @callback console_machine(Machine.t()) :: {:ok, map()} | {:error, String.t()}
  @callback update_machine_status(Repo.t(), list(Multi.changes()), Machine.t()) :: {:ok, String.t()} | {:error, String.t()}

  @callback create_network(Repo.t(), Multi.changes(), Network.t()) :: {:ok, taskid()} | {:error, String.t()}
  @callback add_network_to_machine(Repo.t(), Multi.changes(), []) :: {:ok, String.t()} | {:error, String.t()}

  @callback job_status(Repo.t(), Multi.changes(), Hypervisor.t(), taskid()) :: {:ok, map()} | {:error, String.t()}
end
