defmodule Webapp.DataHelpers do
  @moduledoc """
  Helper functions for working with data.
  """

  @doc """
  Returns the current datetime truncated to the second.

  ## Examples

      iex> datetime_now()
      ~U[2021-09-01 12:00:00]
  """
  def datetime_now() do
    DateTime.utc_now() |> DateTime.truncate(:second)
  end
end
