defmodule Webapp.Guard.HypervisorGuard do
  @moduledoc """
  This module defines the HypervisorGuard process.
  """
  use GenServer

  alias Webapp.Hypervisors

  @doc """
  Starts the HypervisorGuard process.

  ## Options

    * `:interval` - the interval in milliseconds to check the status of the hypervisors.
  """
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(state) do
    schedule(state)
    {:ok, state}
  end

  def handle_info(:update_status, state) do
    update_status()
    schedule(state)
    {:noreply, state}
  end

  # Workaround for the hackney issue: https://github.com/benoitc/hackney/issues/464
  def handle_info({:ssl_closed, _}, state) do
    {:noreply, state}
  end

  def schedule(state) do
    interval = Keyword.get(state, :interval, 5000)
    Process.send_after(self(), :update_status, interval)
  end

  def update_status() do
    for hypervisor <- Hypervisors.list_hypervisors() do
      # This function calls hypervisor only when data in cache exceeded its TTL
      # TTL value is set in ConCache's configuration in application.ex
      Hypervisors.update_hypervisor_os_details(hypervisor)
    end
  end

end
