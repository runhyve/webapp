defmodule Webapp.Guard.JobGuard do
  @moduledoc """
  This module defines the JobGuard process.
  """
  use GenServer

  alias Webapp.Jobs

  @doc """
  Starts the JobGuard process.

  ## Options

    * `:interval` - the interval in milliseconds to check the status of the jobs.
  """
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(state) do
    schedule(state)
    {:ok, state}
  end

  def handle_info(:check_status, state) do
    check_status()
    schedule(state)
    {:noreply, state}
  end

  def schedule(state) do
    interval = Keyword.get(state, :interval, 1000)
    Process.send_after(self(), :check_status, interval)
  end

  def check_status() do
    for job <- Jobs.list_jobs() do
      Jobs.update_status(job)
    end
  end
end
