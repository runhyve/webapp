defmodule Webapp.Guard.MachineGuard do
  @moduledoc """
  This module defines the MachineGuard process.
  """
  use GenServer

  alias Webapp.Machines

  @doc """
  Starts the MachineGuard process.

  ## Options

    * `:interval` - the interval in milliseconds to check the status of the machines.
  """
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(state) do
    schedule(state)
    {:ok, state}
  end

  def handle_info(:check_status, state) do
    check_status()
    schedule(state)
    {:noreply, state}
  end

  # Workaround for the hackney issue: https://github.com/benoitc/hackney/issues/464
  def handle_info({:ssl_closed, _}, state) do
    {:noreply, state}
  end

  def schedule(state) do
    interval = Keyword.get(state, :interval, 5000)
    Process.send_after(self(), :check_status, interval)
  end

  def check_status() do
    for machine <- Machines.list_machines() do
      Machines.update_status(machine)
    end
  end
end
