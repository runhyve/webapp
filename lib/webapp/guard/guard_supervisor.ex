defmodule Webapp.GuardSupervisor do
  @moduledoc """
  This module defines the supervisor for the Guard application.


  """
  use Supervisor

  @doc """
  Starts Guard Supervisor.

  ## Options

    * `:children` - list guard child processes to be supervised or nil when should be disabled.

  ## Examples

      iex> Webapp.GuardSupervisor.start_link(children: [
        {Webapp.Guard.JobGuard, [interval: 10_000]},
      ])
      {:ok, pid}

      iex> Webapp.GuardSupervisor.start_link(children: nil)
      :ignore
  """
  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  @impl true
  def init(opts) do
    children = Keyword.get(opts, :children, [])

    case children do
      nil ->
        :ignore

      children ->
        Supervisor.init(children, strategy: :one_for_one)
    end
  end

end
