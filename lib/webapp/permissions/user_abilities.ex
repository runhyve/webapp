alias Webapp.{
  Accounts.User,
  Accounts.SSHPublicKey,
  Teams.Team,
  Teams,
}

defimpl Canada.Can, for: User do

  ##
  ## Admin permission to access all resources and actions
  ##
  def can?(%User{role: "Administrator"}, _, _model), do: true

  ##
  ## SSHPublicKey permissions
  ##

  # Listing own SSH public keys
  def can?(%User{}, :index, SSHPublicKey), do: true
  # Create new SSH public key
  def can?(%User{}, :new, SSHPublicKey), do: true
  def can?(%User{}, :create, SSHPublicKey), do: true

  # Delete own SSH public key
  def can?(%User{} = user, :delete, %SSHPublicKey{} = ssh_public_key),
    do: user.id == ssh_public_key.user_id

  # Edit own SSH public key
  def can?(%User{} = user, :edit, %SSHPublicKey{} = ssh_public_key),
    do: user.id == ssh_public_key.user_id
  def can?(%User{} = user, :show, %SSHPublicKey{} = ssh_public_key),
    do: user.id == ssh_public_key.user_id


  ##
  ## Team permissions
  ##

  # Listing own teams
  def can?(%User{}, :index, Team), do: true

  # Create new team
  def can?(%User{}, :new, Team), do: true
  def can?(%User{}, :create, Team), do: true

  # Listing team details
  def can?(%User{} = user, :show, %Team{} = team) do
    Teams.member_of_team?(user, team)
  end

  # Invite member to team
  def can?(user, :invite_member, %Team{} = team) do
    Teams.member_with_role?(user, team, "Administrator")
  end
  def can?(user, :send_invite, %Team{} = team) do
    Teams.member_with_role?(user, team, "Administrator")
  end

  # Edit team members
  def can?(user, :edit_member, %Team{} = team) do
    Teams.member_with_role?(user, team, "Administrator")
  end
  def can?(user, :update_member, %Team{} = team) do
    Teams.member_with_role?(user, team, "Administrator")
  end

  # Delete team members
  def can?(user, :delete_member, %Team{} = team) do
    Teams.member_with_role?(user, team, "Administrator")
  end

  ##
  ## Other permissions
  ##
  # Allow to perform any action on own user
  def can?(%User{} = current_user, _action, %User{} = user), do: current_user.id == user.id

  # Disallow any unspecified action
  def can?(_user, _action, _model), do: false
end
