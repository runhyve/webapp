defmodule Webapp.Jobs do
  @moduledoc """
  The Jobs context.
  """

  import Ecto.Query, warn: false
  import Webapp.Hypervisors, only: [get_hypervisor_module: 1]
  alias Webapp.Repo

  alias Ecto.Multi
  alias Webapp.Jobs.Job


  # Delete already finished jobs after 7 days
  @delete_after 60_4800

  @doc """
  Returns the list of job.

  ## Examples

      iex> list_jobs()
      [%Job{}, ...]

  """
  def list_jobs(preloads \\ [:hypervisor]) do
    Job
    |> order_by(desc: :started_at)
    |> Repo.all()
    |> Repo.preload(preloads)
  end

  @doc """
  Returns the list of active jobs..

  ## Examples

      iex> list_jobs()
      [%Job{}, ...]

  """
  def list_active_jobs(preloads \\ [:hypervisor]) do
    Job
    |> where([j], j.last_status not in ["finished", "failed"])
    |> Repo.all()
    |> Repo.preload(preloads)
  end


  @doc """
  Gets a single job.

  Raises `Ecto.NoResultsError` if the Job does not exist.

  ## Examples

      iex> get_job!(123)
      %Job{}

      iex> get_job!(456)
      ** (Ecto.NoResultsError)

  """
  def get_job!(id), do: Repo.get!(Job, id)

  @doc """
  Creates a job.

  ## Examples

      iex> create_job(%{field: value})
      {:ok, %Job{}}

      iex> create_job(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_job(attrs \\ %{}) do
    %Job{}
    |> Job.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a Job.

  ## Examples

      iex> delete_job(job)
      {:ok, %Job{}}

      iex> delete_job(job)
      {:error, %Ecto.Changeset{}}

  """
  def delete_job(%Job{} = job) do
    Repo.delete(job)
  end


  @doc """
  Updates the job status, or remove it when it was finished more than 7 days ago.
  """
  def update_status(%Job{last_status: "finished", finished_at: finished_at} = job) do
    if DateTime.diff(DateTime.utc_now(), finished_at, :second) > @delete_after do
      Repo.delete(job)
    else
      {:ok, job}
    end
  end

  def update_status(%Job{last_status: "failed", finished_at: finished_at} = job) do
    if DateTime.diff(DateTime.utc_now(), finished_at, :second) > @delete_after do
      Repo.delete(job)
    else
      {:ok, job}
    end
  end

  def update_status(%Job{} = job) do
    case check_job_status(job) do
      {:ok, %{job: job}} -> {:ok, job}
      {:error, :hypervisor, error, _} ->
        if is_binary(error) && String.match?(error, ~r/Couldn't get task with id (.*)./) do
          mark_as_failed(job)
          {:error, "Job does not exists on hypervisor"}
        else
          {:error, error}
        end
      {:error, error} -> {:error, error}
    end
  end

  defp check_job_status(job) do
    module = get_hypervisor_module(job)

    try do
      Multi.new()
      |> Multi.run(:hypervisor, module, :job_status,  [job.hypervisor, job.ts_job_id])
      |> Multi.run(:job, __MODULE__, :update_job_status, [job])
      |> Repo.transaction()
    rescue
      UndefinedFunctionError -> {:error, :hypervisor_not_found}
    end
  end

  def update_job_status(_repo, %{hypervisor: %{"state" => "finished", "elevel" => e_level}}, %Job{} = job) do
    Job.finished_changeset(job, %{"e_level" => e_level})
    |> Repo.update()
  end

  def update_job_status(_repo, %{hypervisor: %{"state" => "queued"}}, %Job{} = job) do
    Job.queued_changeset(job)
    |> Repo.update()
  end

  def update_job_status(_repo, %{hypervisor: %{"state" => "running"}}, %Job{} = job) do
    Job.running_changeset(job)
    |> Repo.update()
  end

  def update_job_status(_repo, %{hypervisor: %{"state" => state}}, %Job{} = job) do
    Job.update_changeset(job, %{"last_status" => state})
    |> Repo.update()
  end

  defp mark_as_failed(job) do
    Job.failed_changeset(job, %{"e_level" => 127})
    |> Repo.update()
  end
end
