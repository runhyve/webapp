defmodule Webapp.Jobs.Job do
  use Ecto.Schema
  import Ecto.Changeset
  import Webapp.DataHelpers, only: [datetime_now: 0]

  alias Webapp.Hypervisors.Hypervisor

  schema "jobs" do
    field :last_status, :string
    field :ts_job_id, :integer
    field(:e_level, :integer, default: nil)
    belongs_to(:hypervisor, Hypervisor)
    has_one(:machine, through: [:hypervisor, :machines])

    timestamps(type: :utc_datetime)
    field(:started_at, :utc_datetime, default: nil)
    field(:finished_at, :utc_datetime, default: nil)
  end

  @doc false
  def changeset(job, attrs) do
    job
    |> cast(attrs, [:hypervisor_id, :ts_job_id, :last_status, :e_level, :started_at, :finished_at])
    |> validate_required([:hypervisor_id, :ts_job_id])
    |> assoc_constraint(:hypervisor)
  end

  def queued_changeset(job) do
    job
    |> cast(%{}, [])
    |> put_change(:last_status, "queued")
  end

  def running_changeset(job) do
    job
    |> cast(%{}, [])
    |> put_change(:last_status, "running")
    |> maybe_update_started_at()
  end

  defp maybe_update_started_at(changeset) do
    now = datetime_now()
    if get_field(changeset, :started_at, nil) == nil do
      put_change(changeset, :started_at, now)
    else
      changeset
    end
  end

  def update_changeset(job, attrs) do
    cast(job, attrs, [:last_status, :e_level])
  end

  def finished_changeset(job, attrs) do
    update_changeset(job, attrs)
    |> mark_as_finished()
  end

  def failed_changeset(job, attrs) do
    update_changeset(job, attrs)
    |> mark_as_finished("failed")
  end

  defp mark_as_finished(changeset, status \\ "finished") do
    changeset
    |> put_change(:finished_at, datetime_now())
    |> put_change(:last_status, status)
  end
end
