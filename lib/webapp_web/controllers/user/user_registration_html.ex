defmodule WebappWeb.UserRegistrationHTML do
  use WebappWeb, :html

  embed_templates "user_registration_html/*"
end
