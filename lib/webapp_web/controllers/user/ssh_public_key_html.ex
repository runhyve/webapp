defmodule WebappWeb.SSHPublicKeyHTML do
  use WebappWeb, :html

  embed_templates "ssh_public_key_html/*"

  @doc """
  Renders a movie form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def ssh_public_key_form(assigns)
end
