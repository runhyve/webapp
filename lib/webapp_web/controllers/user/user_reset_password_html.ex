defmodule WebappWeb.UserResetPasswordHTML do
  use WebappWeb, :html

  embed_templates "user_reset_password_html/*"
end
