defmodule WebappWeb.UserConfirmationHTML do
  use WebappWeb, :html

  embed_templates "user_confirmation_html/*"
end
