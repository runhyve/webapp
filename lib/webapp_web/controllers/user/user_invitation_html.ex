defmodule WebappWeb.UserInvitationHTML do
  use WebappWeb, :html

  embed_templates "user_invitation_html/*"
end
