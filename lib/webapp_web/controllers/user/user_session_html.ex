defmodule WebappWeb.UserSessionHTML do
  use WebappWeb, :html

  embed_templates "user_session_html/*"
end
