defmodule WebappWeb.UserRegistrationController do
  use WebappWeb, :controller_ng

  alias Webapp.Accounts
  alias Webapp.Accounts.Registration

  def new(conn, _params) do
    changeset = Accounts.change_registration(%Registration{})
    render(conn, :new, changeset: changeset)
  end

  def create(conn, %{"registration" => registration_params}) do
    changeset = Registration.changeset(%Registration{}, registration_params)

    case Accounts.register_user(registration_params) do
      {:ok, %{user: user}} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &url(~p"/users/confirm/#{&1}")
          )

        conn
        |> put_flash(:info, "User created successfully. Please check your email to confirm your account.")
        |> redirect(to: ~p"/users/log_in")

      {:error, :registration, multi_changeset, _changes} ->
        changeset = %{changeset | errors: multi_changeset.errors, valid?: false, action: :insert}
        render(conn, "new.html", changeset: changeset)

      {:error, :user, multi_changeset, _changes} ->
        changeset = Registration.copy_changeset_errors(multi_changeset, changeset, "user")
        render(conn, "new.html", changeset: %{changeset | action: :insert})

      {:error, :team, multi_changeset, _changes} ->
        changeset = Registration.copy_changeset_errors(multi_changeset, changeset, "team")
        render(conn, "new.html", changeset: %{changeset | action: :insert})
    end
  end
end
