defmodule WebappWeb.UserSettingsHTML do
  use WebappWeb, :html

  embed_templates "user_settings_html/*"
end
