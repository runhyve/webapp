defmodule WebappWeb.UserInvitationController do
  use WebappWeb, :controller_ng

  alias Webapp.Accounts
  alias WebappWeb.UserAuth

  plug :get_user_by_reset_invite_token when action in [:edit, :update]

  def edit(conn, _params) do
    render(conn, :edit, changeset: Accounts.change_user_invite(conn.assigns.user))
  end

  def update(conn, %{"user" => user_params}) do
    case Accounts.accept_invite_user(conn.assigns.token, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Invite accepted successfully.")
        |> put_session(:user_return_to, ~p"/users/settings")
        |> UserAuth.log_in_user(user)

      {:error, changeset} ->
        render(conn, :edit, changeset: changeset)

      :error ->
        conn
        |> put_flash(:error, "Invite link is invalid or it has expired.")
        |> redirect(to: ~p"/")
    end
  end

  def get_user_by_reset_invite_token(conn, _opts) do
    %{"token" => token} = conn.params

    if user = Accounts.get_user_by_invite_token(token) do
      user = Accounts.get_user!(user.id, [:teams])
      conn
      |> assign(:user, user)
      |> assign(:token, token)
    else
      conn
      |> put_flash(:error, "Invite link is invalid or it has expired.")
      |> redirect(to: ~p"/")
      |> halt()
    end
  end
end
