defmodule WebappWeb.Admin.UserController do
  use WebappWeb, :controller

  alias Webapp.{
    Accounts,
    Accounts.User,
    Accounts.Registration
  }

  plug :load_and_authorize_resource,
    model: User,
    non_id_actions: [:index, :create, :new],
    preload: [:teams]

  def index(conn, _) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_registration(%Registration{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"registration" => registration_params}) do
    changeset = Registration.changeset(%Registration{}, registration_params)

    case Accounts.register_user(registration_params) do
      {:ok, %{user: user}} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &url(~p"/users/confirm/#{&1}")
          )

        conn
        |> put_flash(:info, "User created successfully. Confirmatiom was sent to user email address.")
        |> redirect(to: ~p"/admin/users/#{user.id}")

      {:error, :registration, multi_changeset, _changes} ->
        changeset = %{changeset | errors: multi_changeset.errors, valid?: false, action: :insert}
        render(conn, "new.html", changeset: changeset)

      {:error, :user, multi_changeset, _changes} ->
        changeset = Registration.copy_changeset_errors(multi_changeset, changeset, "user")
        render(conn, "new.html", changeset: %{changeset | action: :insert})

      {:error, :team, multi_changeset, _changes} ->
        changeset = Registration.copy_changeset_errors(multi_changeset, changeset, "team")
        render(conn, "new.html", changeset: %{changeset | action: :insert})
    end
  end

  def show(conn, _params) do
    user = conn.assigns[:user]
    render(conn, "show.html", user: user)
  end
end
