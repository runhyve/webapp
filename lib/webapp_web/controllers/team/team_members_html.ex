defmodule WebappWeb.TeamMembersHTML do
  use WebappWeb, :html

  embed_templates "team_members_html/*"
end
