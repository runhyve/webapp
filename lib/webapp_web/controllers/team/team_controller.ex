defmodule WebappWeb.TeamController do
  use WebappWeb, :controller_ng

  alias Webapp.Teams
  alias Webapp.Teams.Team

  plug :load_and_authorize_resource,
    model: Team,
    non_id_actions: [:index],
    preload: [members: :user]

  def index(%Conn{assigns: %{current_user: user}} = conn, _params) do
    render(conn, :index, teams: user.teams)
  end

  def new(conn, _params) do
    changeset = Teams.change_team(%Team{})
    render(conn, :new, changeset: changeset)
  end

  def create(%Conn{assigns: %{current_user: user}} = conn, %{"team" => team_params}) do
    team_params =
      Map.merge(team_params, %{"members" => [%{"user_id" => user.id, "role" => "Administrator"}]})

    case Teams.create_team(team_params) do
      {:ok, _team} ->
        conn
        |> put_flash(:info, "Team created successfully.")
        |> redirect(to: ~p"/users/teams")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    team = conn.assigns[:team]
    render(conn, :show, team: team)
  end
end
