defmodule WebappWeb.TeamMembersController do
  use WebappWeb, :controller_ng

  alias Webapp.Accounts
  alias Webapp.Teams
  alias Webapp.Teams.Team
  alias Webapp.Teams.Invitation

  plug :load_and_authorize_resource,
    model: Team,
    id_name: "team_id",
    preload: [members: :user],
    required: true

  plug :get_member when action not in [:invite_member, :send_invite]
  plug :ensure_can_delete_member when action in [:delete_member]

  def invite_member(conn, _params) do
    team = conn.assigns[:team]
    render(conn, :new, team: team, changeset: Invitation.changeset(%Invitation{}))
  end

  def send_invite(conn, %{"invitation" => %{"email" => email}}) do
    team = conn.assigns[:team]

    case Teams.invite_member(email, team) do
      {:ok, user} ->
        Accounts.deliver_user_invitation_instructions(user, team, &url(~p"/users/join/#{&1}"))

        action_notification = case user.confirmed_at do
          nil -> "Invitation sent to #{email}."
          _ -> "User #{user.name} has been added to the team."
        end

        conn
        |> put_flash(:info, action_notification)
        |> redirect(to: ~p"/users/teams/#{team.id}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, team: team, changeset: changeset)

      {:error, error} ->
        conn
        |> put_flash(:error, error)
        |> render(:new, team: team, changeset: Invitation.changeset(%Invitation{}))
    end
  end

  def edit_member(conn, _params) do
    changeset = Teams.change_member(conn.assigns[:member])

    render(conn, :edit, changeset: changeset)
  end

  def update_member(conn, %{"member" => member_params}) do
    member = conn.assigns[:member]

    case Teams.update_member(member, member_params) do
      {:ok, member} ->
        conn
        |> put_flash(:info, "Member updated successfully.")
        |> redirect(to: ~p"/users/teams/#{member.team_id}")

      {:error, changeset} ->
        render(conn, :edit, changeset: changeset)
    end
  end

  def delete_member(conn, _params) do
    team = conn.assigns[:team]
    member = conn.assigns[:member]

    {:ok, _member} = Teams.delete_member(member)

    conn
    |> put_flash(:info, "Member #{member.user.name} deleted successfully.")
    |> redirect(to: ~p"/users/teams/#{team}")
  end

  defp get_member(conn, _opts) do
    team = conn.assigns[:team]
    %{"id" => id} = conn.params

    member = Enum.find(team.members, fn member -> member.id == String.to_integer(id) end)
    case member do
      nil ->
        conn
        |> put_flash(:error, "Member not found.")
        |> redirect(to: ~p"/users/teams/#{team.id}")
        |> halt()

      _ ->
        conn
        |> assign(:member, member)
    end
  end

  defp ensure_can_delete_member(conn, _opts) do
    team = conn.assigns[:team]

    admins = Enum.filter(team.members, fn member -> member.role == "Administrator" end)

    if Enum.count(admins) > 1 || hd(admins).id != conn.assigns.member.id do
      conn
    else
      conn
      |> put_flash(:error, "Team must have at least one administrator.")
      |> redirect(to: ~p"/users/teams/#{team.id}")
      |> halt()
    end
  end
end
