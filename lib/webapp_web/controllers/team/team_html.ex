defmodule WebappWeb.TeamHTML do
  use WebappWeb, :html

  embed_templates "team_html/*"
end
