defmodule WebappWeb.Admin.IpPoolView do
  use WebappWeb, :view

  def networks_select_options(networks) do
    for network <- networks, do: {"#{network.name}@#{network.hypervisor.name}", network.id}
  end

  def count_used_ips(ips) do
    Enum.count(ips, fn ip -> ipv4_is_available?(ip) == false end)
  end

  def ipv4_is_available?(ip) do
    ip.machine_id == nil && ip.reserved == false
  end
end
