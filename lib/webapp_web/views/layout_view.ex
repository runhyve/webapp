defmodule WebappWeb.LayoutView do
  use WebappWeb, :view

  alias Webapp.{Teams.Team}

  def version do
    "#{Application.spec(:webapp, :vsn)}-#{Mix.Project.config()[:vcs_version]}"
  end

  def switch_team_path(conn, %Team{} = team) do
    "/" <> team.namespace <> "/" <> Enum.join(conn.path_info, "/")
  end
end
