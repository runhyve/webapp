defmodule WebappWeb.ViewHelpers do
  alias Webapp.{
    Machines,
    Machines.Machine
  }

  def action_css_class(%Machine{} = machine, action) do
    case Machines.machine_can_do?(machine, action) do
      false -> "action-#{action} is-hidden"
      _ -> "action-#{action}"
    end
  end

  def action_css_class(_, action), do: "action-#{action}"
end
