defmodule WebappWeb.Router do
  use WebappWeb, :router

  import WebappWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
    plug WebappWeb.TeamContext
  end

  pipeline :new_layout do
    plug :put_root_layout, html: {WebappWeb.Layouts, :root}
  end

  pipeline :authenticated do
    plug :require_authenticated_user
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug WebappWeb.HypervisorTokenAuth
  end

  scope "/api/v1", WebappWeb.ApiV1 do
    pipe_through :api

    resources "/distributions", DistributionController, except: [:new, :edit]
  end

  scope "/", WebappWeb do
    pipe_through :browser
    get "/", PageController, :index
    get "/health", PageController, :health
  end

  scope "/", WebappWeb do
    pipe_through [:browser, :authenticated, :redirect_if_user_must_update_password]

    # Machine
    resources "/machines", MachineController, except: [:new, :create]
    get "/machines/:id/console", MachineController, :console
    post "/machines/:id/start", MachineController, :start
    post "/machines/:id/stop", MachineController, :stop
    post "/machines/:id/restart", MachineController, :restart
    post "/machines/:id/poweroff", MachineController, :poweroff
    get "/hypervisors/:hypervisor_id/machines/new", MachineController, :new
    post "/hypervisors/:hypervisor_id/machines/create", MachineController, :create
  end

  scope "/admin", WebappWeb.Admin, as: :admin do
    pipe_through [:browser, :authenticated, :redirect_if_user_must_update_password]

    resources "/hypervisors", HypervisorController do
      resources "/networks", NetworkController, only: [:new, :create, :index]
    end
    get "/hypervisors/:id/machines", HypervisorController, :index_machines

    resources "/regions", RegionController
    resources "/machines", MachineController, only: [:index]
    resources "/plans", PlanController
    resources "/networks", NetworkController, except: [:new, :create, :index]
    resources "/ip_pools", IpPoolController, except: [:edit, :update, :delete]
    resources "/users", UserController, except: [:edit, :update, :delete]
    resources "/teams", TeamController, only: [:index]
    resources "/jobs", JobController, only: [:index]
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:webapp, :dev_routes) do
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
      live_dashboard "/dashboard", metrics: WebappWeb.Telemetry
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", WebappWeb do
  #   pipe_through :api
  # end

  ## Authentication routes

  scope "/", WebappWeb do
    pipe_through [:browser, :new_layout, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", WebappWeb do
    pipe_through [:browser, :new_layout, :authenticated, :redirect_if_user_must_update_password]
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
    resources "/users/teams", TeamController, except: [:delete] do
      get "/members/new", TeamMembersController, :invite_member
      post "/members", TeamMembersController, :send_invite
      delete "/members/:id", TeamMembersController, :delete_member
      get "/members/:id/edit", TeamMembersController, :edit_member
      put "/members/:id", TeamMembersController, :update_member
    end
    resources "/users/keys", SSHPublicKeyController, except: [:edit]
  end

  scope "/", WebappWeb do
    pipe_through [:browser, :new_layout, :authenticated]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
  end

  scope "/", WebappWeb do
    pipe_through [:browser, :new_layout]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
    get "/users/join/:token", UserInvitationController, :edit
    put "/users/join/:token", UserInvitationController, :update
  end
end
