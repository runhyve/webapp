defmodule WebappWeb.RouterHelper do
  @moduledoc """
  Helper functions for generating paths in the router.
  """

  alias WebappWeb.Router.Helpers, as: Routes
  alias Webapp.Teams.Team

  @doc """
  Generates a path for a team context.

  Might be used with a team struct, a conn with a team in assigns or even with team namespace string.

  ## Examples

      iex> team_path(%Webapp.Teams.Team{namespace: "team1"}, ~p"/machines")
      "/team1/machines"

      iex> team_path(conn, ~p"/machines")
      "/team2/machines"

      iex> team_path("team3", ~p"/machines")
      "/team3/machines"

      iex> team_path(nil, ~p"/machines")
      "/machines"
  """
  def team_path(%Team{namespace: namespace}, path) do
    "/" <> namespace <> path
  end

  def team_path(%Plug.Conn{assigns: %{current_team: team}}, path) do
    "/" <> team.namespace <> path
  end

  def team_path(team, path) when is_binary(team) do
    "/" <> team <> path
  end

  def team_path(_, path), do: path

  @doc """
  Generates a path for a team context using the provided helper.

  Takes team from conn assigns and prepends it to the generated path.

  ## Examples

      iex> team_path(:machine_path, %Plug.Conn{assigns: %{current_team: %Webapp.Teams.Team{namespace: "team2"}}}, :index, [])
      "/team2/machines"

      iex> team_path(:machine_path, %Plug.Conn{}, :index, [])
      "/machines"

      iex> team_path(:machine_path, nil, :index, [])
      "/machines"
  """
  def team_path(helper, conn, action, opts \\ [])

  def team_path(
        helper,
        %Plug.Conn{assigns: %{current_team: %Team{} = team}} = conn,
        action,
        opts
      ) do
    path = apply(Routes, helper, [conn, action, opts])
    "/" <> team.namespace <> path
  end

  def team_path(helper, conn, action, opts) do
    apply(Routes, helper, [conn, action, opts])
  end
end
