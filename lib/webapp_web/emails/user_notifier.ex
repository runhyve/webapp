defmodule WebappWeb.Emails.UserNotifier do
  use WebappWeb, :email

  alias Webapp.Mailer

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject, body) do


    email = new()
      |> to(recipient)
      |> from({System.get_env("SMTP_FROM_DISPLAY_NAME") || "Runhyve Webapp", System.get_env("SMTP_FROM") || "noreply@runhyve.app"})
      |> subject(subject)
      |> text_body(body)

    with {:ok, _metadata} <- Mailer.deliver(email) do
      {:ok, email}
    end
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def deliver_confirmation_instructions(user, url) do
    deliver(user.email, "Confirmation instructions", """

    ==============================

    Hi #{user.email},

    You can confirm your account by visiting the URL below:

    #{url}

    If you didn't create an account with us, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to reset a user password.
  """
  def deliver_reset_password_instructions(user, url) do
    deliver(user.email, "Reset password instructions", """

    ==============================

    Hi #{user.email},

    You can reset your password by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to update a user email.
  """
  def deliver_update_email_instructions(user, url) do
    deliver(user.email, "Update email instructions", """

    ==============================

    Hi #{user.email},

    You can change your email by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to accept invitaion.
  """
  def deliver_invitation_instructions(user, team, url) do
    deliver(user.email, "Invitation instructions", """

    ==============================

    Hi #{user.email},

    You have been invited to join the #{team.name} team.
    You can confirm your account by visiting the URL below:

    #{url}

    ==============================
    """)
  end

  @doc """
  Deliver notification to new team member.
  """
  def deliver_invitation_notification(user, team) do
    deliver(user.email, "You have been added to a new team", """

    ==============================

    Hi #{user.email},

    You have been added to the #{team.name} team.

    ==============================
    """)
  end

end
