defmodule WebappWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use WebappWeb, :controller
      use WebappWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def static_paths, do: ~w(assets fonts images favicon.ico robots.txt)

  def controller do
    quote do
      use Phoenix.Controller, namespace: WebappWeb

      import Plug.Conn
      import WebappWeb.Gettext
      import Canary.Plugs
      alias WebappWeb.Router.Helpers, as: Routes
      alias Plug.Conn

      unquote(verified_routes())
    end
  end

  def controller_ng do
    quote do
      alias Plug.Conn
      import Canary.Plugs
      use Phoenix.Controller,
        formats: [:html, :json],
        layouts: [html: WebappWeb.Layouts]

      use Gettext, backend: WebappWeb.Gettext

      import Plug.Conn

      unquote(verified_routes())
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "lib/webapp_web/templates", pattern: "**/*", namespace: WebappWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      import Phoenix.HTML
      import Phoenix.HTML.Form
      use PhoenixHTMLHelpers

      import WebappWeb.ErrorHelpers
      import WebappWeb.Gettext
      alias WebappWeb.Router.Helpers, as: Routes

      import Canada.Can, only: [can?: 3]
      import PhoenixActiveLink
      import WebappWeb.ViewHelpers

      unquote(verified_routes())
    end
  end

  def html do
    quote do
      use Phoenix.Component

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_csrf_token: 0, view_module: 1, view_template: 1]
      # Core UI components
      import WebappWeb.CoreComponents

      import Canada.Can, only: [can?: 3]

      # Include general helpers for rendering HTML
      unquote(html_helpers())
    end
  end

  defp html_helpers do
    quote do
      # Translation
      use Gettext, backend: WebappWeb.Gettext

      # HTML escaping functionality
      import Phoenix.HTML

      import WebappWeb.CoreComponents

      # Shortcut for generating JS commands
      alias Phoenix.LiveView.JS

      # Routes generation with the ~p sigil
      unquote(verified_routes())
    end
  end

  def router do
    quote do
      use Phoenix.Router

      # Import common connection and controller functions to use in pipelines
      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import WebappWeb.Gettext
    end
  end

  def model do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
    end
  end

  def email do
    quote do
      import Swoosh.Email
      alias Webapp.Mailer


      alias WebappWeb.Router.Helpers, as: Routes
      alias WebappWeb.Endpoint
    end
  end

  def verified_routes do
    quote do
      use Phoenix.VerifiedRoutes,
        endpoint: WebappWeb.Endpoint,
        router: WebappWeb.Router,
        statics: WebappWeb.static_paths()

      import WebappWeb.RouterHelper
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
