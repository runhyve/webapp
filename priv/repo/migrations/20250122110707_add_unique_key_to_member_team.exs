defmodule Webapp.Repo.Migrations.AddUniqueKeyToMemberTeam do
  use Ecto.Migration

  def change do
    create unique_index(:members, [:team_id, :user_id])
  end
end
