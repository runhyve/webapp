defmodule Webapp.Repo.Migrations.AddForcePasswordUpdateToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :force_password_update, :boolean, default: false
    end
  end
end
