defmodule Webapp.Repo.Migrations.RenameResetSentAtTimestamp do
  use Ecto.Migration

  def change do
    # After dropping phauxth reset_sent_at field is no longer needed
    # Let's use it for joined_at instead to keep track of when the user joined
    # after invitation
    rename table(:users), :reset_sent_at, to: :joined_at
  end
end
