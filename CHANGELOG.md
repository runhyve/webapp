# Changelog

All notable changes to this project will be documented in this file.

## In progress

### Added
- Job status `failed`  [!118](https://gitlab.com/runhyve/webapp/-/merge_requests/118)
- Old jobs should be removed after 7 days [!118](https://gitlab.com/runhyve/webapp/-/merge_requests/118)
- :telemetry and phoenix_live_dashboard  [!119](https://gitlab.com/runhyve/webapp/-/merge_requests/119)
- Metrics for hypervisor backend [!119](https://gitlab.com/runhyve/webapp/-/merge_requests/119)

## 0.1.2 - 2025-01-30
Use UUIDs as vm name
Ref: [!98](https://gitlab.com/runhyve/webapp/-/merge_requests/98)

### Breaking changes
> Warning:
> Machines created with old naming convention `<team_id>_<machine_name>` won't > be visible for the runhyve unless the uuid column will be erased.

You can use empty all uuids with psql:
```
UPDATE machines SET uuid = NULL;
```

or iex -S mix:
```
Webapp.Repo.update_all(Webapp.Machines.Machine, set: [uuid: nil])
```

### Added
- A lot of test coverage for `Webapp.Machines` and `WebappWeb.MachineController`
- [Mox](https://hexdocs.pm/mox/Mox.html) for mocking hypervisor backend

### Changed
- From now `machine.uuid` will be used to perform any taks on hypervisor

## 0.1.1 - 2025-01-21

Upgrade to Phoenix 1.7
Ref: [!111](https://gitlab.com/runhyve/webapp/-/merge_requests/111)

### Added

- `Phoenix.VerifiedRoutes`
- `Phoenix.Component` which is replacement for `Phoenix.View`
- Added TailwindCSS which will be replace Bluma

### Changed

- All User related controllers now using components, and TailwindCSS
- All Team related controllers now using components, and TailwindCSS
- All SSH public key related controllers now using components, and TailwindCSS

### Removed

- Removed `phauxth` from deps, instead we are used built-in generator for authentication.