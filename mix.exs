defmodule Webapp.MixProject do
  use Mix.Project

  def project do
    [
      app: :webapp,
      version: "0.1.2",
      vcs_version: vcs_version(),
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      # TODO: Uncomment on real :prod!
      # start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Webapp.Application, []},
      extra_applications: extra_applications()
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.0"},
      {:phoenix_pubsub, "~> 2.0"},
      {:ecto_sql, "~> 3.0"},
      {:phoenix_ecto, "~> 4.5"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 4.1"},
      {:phoenix_view, "~> 2.0"},
      {:phoenix_live_reload, "~> 1.5", only: :dev},
      {:phoenix_live_view, "~> 1.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.1"},
      {:argon2_elixir, "~> 4.1"},
      {:swoosh, "~> 1.5"},
      {:gen_smtp, "~> 1.1"},
      {:canary, git: "https://github.com/runhyve/canary.git"},
      {:plug_cowboy, "~> 2.3"},
      {:plug, "~> 1.7"},
      {:excoveralls, "~> 0.10", only: [:dev, :test]},
      {:junit_formatter, "~> 3.4", only: [:test]},
      {:httpoison, "~> 2.2"},
      {:phoenix_active_link, github: "bizneo/phoenix-active-link"},
      {:table_rex, "~> 4.0.0"},
      {:ecto_network, "~> 1.5.0"},
      {:credo, "~> 1.7.0", only: [:dev, :test], runtime: false},
      {:inet_cidr, "~> 1.0.0"},
      {:iptools, "~> 0.0.2"},
      {:con_cache, "~> 1.0"},
      {:bypass, "~> 2.0", only: :test},
      {:ex_machina, "~> 2.7", only: :test},
      {:faker, "~> 0.16", only: :test},
      {:mox, "~> 1.0", only: :test},
      {:sentry, "~> 10.0"},
      {:hackney, "~> 1.8"},
      {:esbuild, "~> 0.2", runtime: Mix.env() == :dev},
      {:dart_sass, "~> 0.3", runtime: Mix.env() == :dev},
      {:tailwind, "~> 0.2", runtime: Mix.env() == :dev},
      {:heroicons,
       github: "tailwindlabs/heroicons",
       tag: "v2.1.1",
       sparse: "optimized",
       app: false,
       compile: false,
       depth: 1},
       {:phoenix_live_dashboard, "~> 0.8.3"},
       {:telemetry_metrics, "~> 1.0"},
       {:telemetry_poller, "~> 1.0"},
       {:ecto_psql_extras, "~> 0.6"},
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "assets.setup": ["tailwind.install --if-missing", "esbuild.install --if-missing", "sass.install --if-missing"],
      "assets.build": ["tailwind default", "esbuild default", "sass default"],
      "assets.deploy": [
        "tailwind default --minify",
        "esbuild default --minify",
        "sass default --no-source-map --style=compressed"
      ],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end

  # Combines version with commit hash, environment etc.
  defp vcs_version do
    if Mix.env() == :prod do
      with {:ok, hash} <- File.read(".vcs_version") do
        String.trim(hash)
      else
        {:error, _} -> "prod"
      end
    else
      "dev"
    end
  end

  defp extra_applications() do
    case Mix.env() do
      :dev -> [:logger, :runtime_tools, :con_cache, :ssh, :os_mon]
      _ -> [:logger, :runtime_tools, :con_cache, :ssh]
    end
  end
end
