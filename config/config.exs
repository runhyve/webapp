# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :webapp,
  ecto_repos: [Webapp.Repo]

# Configures the endpoint
config :webapp, WebappWeb.Endpoint,
  url: [
    host: System.get_env("WEBAPP_HOST") || "localhost",
    port: System.get_env("WEBAPP_PORT") || 4000
  ],
  secret_key_base: "doiKkY1e11RF0fwHberwtJCjErEC24XipePbdyne5HYn5mBvGf+cEQ+ou+CkEpBy",
  render_errors: [view: WebappWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: Webapp.PubSub,
  live_view: [signing_salt: "J8jK6QmL"]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.4.3",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/appng.css
      --output=../priv/static/assets/appng.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.17.11",
  default: [
    args:
      ~w(js/app.js js/admin.js js/hypervisor.js js/ip_pool.js js/machine.js js/team.js --bundle --target=es2016 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :dart_sass,
  version: "1.58.0",
  default: [
    args: ~w(css/app.scss ../priv/static/assets/app.css),
    cd: Path.expand("../assets", __DIR__)
  ]

# Mailer configuration
config :webapp, Webapp.Mailer, adapter: Swoosh.Adapters.Local

config :canary,
  repo: Webapp.Repo,
  unauthorized_handler: {WebappWeb.UserAuth, :handle_unauthorized},
  not_found_handler: {WebappWeb.UserAuth, :handle_not_found}

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix and Ecto
config :phoenix, :json_library, Jason
config :postgrex, :json_library, Jason

config :phoenix_active_link, :defaults,
  wrap_tag: :li,
  class_active: "is-active",
  class_inactive: ""

config :webapp, Webapp.Notifications,
  enabled_modules: [Webapp.Notifications.NotifyConsole],
  slack_webhook_url: "",
  slack_channel: "",
  slack_username: "Runhyve"

config :webapp, ConCache,
  name: :rh_cache,
  ttl_check_interval: :timer.seconds(300),
  global_ttl: :timer.seconds(300)

config :webapp, Webapp.GuardSupervisor,
  children: [
    {Webapp.Guard.JobGuard, [interval: 1_000]},
    {Webapp.Guard.MachineGuard, [interval: 5_000]},
    {Webapp.Guard.HypervisorGuard, [interval: 30_000]},
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
