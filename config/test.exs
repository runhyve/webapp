import Config

# Only in tests, remove the complexity from the password hashing algorithm
config :argon2_elixir, t_cost: 1, m_cost: 8

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :webapp, WebappWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# Enable helpful, but potentially expensive runtime checks
config :phoenix_live_view,
  enable_expensive_runtime_checks: true

# Configure your database
config :webapp, Webapp.Repo,
  database: System.get_env("POSTGRES_DB") || "webapp_test",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# Mailer test configuration
config :webapp, Webapp.Mailer, adapter: Swoosh.Adapters.Test

# Disable Guards on test
config :webapp, Webapp.GuardSupervisor,
  children: nil

# Disable swoosh api client as it is only required for production adapters
config :swoosh, :api_client, false
