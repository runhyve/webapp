defmodule WebappWeb.UserInvitationControllerTest do
  use WebappWeb.ConnCase, async: true

  alias Webapp.Accounts
  alias Webapp.Accounts.UserToken
  alias Webapp.Repo

  setup do
    team = %Webapp.Teams.Team{name: "Test team", namespace: "test-team"}
    {:ok, user} = Accounts.invite_user(unique_user_email())
    token = extract_user_token(fn url ->
      Accounts.deliver_user_invitation_instructions(user, team, url)
    end)

    %{
      user: user,
      team: team,
      token: token
    }
  end

  describe "GET /users/join/:token" do
    test "renders the join page when toke is valid", %{conn: conn, token: token} do
      token_path = ~p"/users/join/#{token}"
      conn = get(conn, token_path)
      response = html_response(conn, 200)
      assert response =~ "Accept invitation"

      assert response =~ "action=\"#{token_path}\""
    end

    test "redirects to home page when token is invalid", %{conn: conn} do
      conn = get(conn, ~p"/users/join/oops")
      assert redirected_to(conn) == ~p"/"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Invite link is invalid or it has expired"
    end

    test "redirect to home page when token has expired", %{conn: conn, token: token} do
      {1, nil} = Repo.update_all(UserToken, set: [inserted_at: ~N[2020-01-01 00:00:00]])

      conn = get(conn, ~p"/users/join/#{token}")
      assert redirected_to(conn) == ~p"/"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Invite link is invalid or it has expired"
    end
  end

  describe "PUT /users/join/:token" do
    test "joins the given token once", %{conn: conn, user: user, token: token} do
      join_attrs = %{
        "user" => %{
          "password" => "new valid password",
          "password_confirmation" => "new valid password"
        }
      }

      conn = put(conn, ~p"/users/join/#{token}", join_attrs)
      assert redirected_to(conn) == ~p"/users/settings"

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Invite accepted successfully"

      assert Accounts.get_user_by_email_and_password(user.email, "new valid password")


      # Now do a logged in request and assert user is logged in
      conn = get(conn, ~p"/")
      assert html_response(conn, 200)
      assert conn.assigns.current_user.id == user.id

      # Invite token should be deleted
      conn = get(build_conn(), ~p"/users/join/#{token}")
      assert redirected_to(conn) == ~p"/"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Invite link is invalid or it has expired"
    end

    test "does not joins with invalid data", %{conn: conn, user: user, token: token} do
      join_attrs = %{
        "user" => %{
          "password" => "new valid password",
          "password_confirmation" => "new different password"
        }
      }

      conn = put(conn, ~p"/users/join/#{token}", join_attrs)

      assert html_response(conn, 200) =~ "something went wrong"
      assert user == Accounts.get_user!(user.id)
    end

    test "does not joins with invalid token", %{conn: conn, user: user} do
      conn = get(conn, ~p"/users/join/oops")
      assert redirected_to(conn) == ~p"/"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Invite link is invalid or it has expired"

      refute Accounts.get_user!(user.id).confirmed_at
    end
  end
end
