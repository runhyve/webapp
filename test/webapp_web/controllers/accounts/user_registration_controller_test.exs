defmodule WebappWeb.UserRegistrationControllerTest do
  use WebappWeb.ConnCase, async: true

  describe "GET /users/register" do
    test "renders registration page", %{conn: conn} do
      conn = get(conn, ~p"/users/register")
      response = html_response(conn, 200)
      assert response =~ "Register"
      assert response =~ ~p"/users/log_in"
      assert response =~ ~p"/users/register"
    end

    test "redirects if already logged in", %{conn: conn} do
      conn = conn |> log_in_user(user_fixture()) |> get(~p"/users/register")

      assert redirected_to(conn) == ~p"/"
    end
  end

  describe "POST /users/register" do
    @tag :capture_log
    test "creates account and redirect to the login page", %{conn: conn} do
      email = unique_user_email()

      conn =
        post(conn, ~p"/users/register", %{
          "registration" => valid_user_attributes(user_email: email)
        })

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
        "User created successfully. Please check your email to confirm your account."
      assert redirected_to(conn) == ~p"/users/log_in"
    end

    test "render errors for invalid data", %{conn: conn} do
      conn =
        post(conn, ~p"/users/register", %{
          "registration" => %{
            "user_name" => "name",
            "user_email" =>
            "with spaces",
            "user_password" => "short",
            "team_name" => "some name",
            "team_namespace" => "some-namespace"
          }
        })

      response = html_response(conn, 200)
      assert response =~ "Register"
      assert response =~ "must have the @ sign and no spaces"
      assert response =~ "should be at least 7 character(s)"
    end

    test "render errors when email is already taken", %{conn: conn} do
      email = unique_user_email()

      conn =
        post(conn, ~p"/users/register", %{
          "registration" => valid_user_attributes(user_email: email)
        })

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "User created successfully. Please check your email to confirm your account."
      assert redirected_to(conn) == ~p"/users/log_in"

      conn =
        post(conn, ~p"/users/register", %{
          "registration" => valid_user_attributes(user_email: email)
        })

      response = html_response(conn, 200)
      assert response =~ "Register"
      assert response =~ "has already been taken"
    end

  end
end
