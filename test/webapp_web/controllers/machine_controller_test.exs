defmodule WebappWeb.MachineControllerTest do
  use WebappWeb.ConnCase

  import Mox

  setup :verify_on_exit!

  describe "index" do
    setup :register_and_log_in_user

    setup %{conn: conn} do
      hypervisor = insert(:hypervisor)

      # Perform a request to fetch all assigns form TeamContext plug
      conn = get(conn, ~p"/")

      {:ok, conn: conn, hypervisor: hypervisor}
    end

    test "lists all team machines", %{conn: conn, team: team} do
      conn = get(conn, team_path(team, ~p"/machines"))
      assert html_response(conn, 200) =~ "Listing Machines"
      assert html_response(conn, 200) =~ "Empty here, add a machine"

      machine = insert(:machine, team: team)
      conn = get(conn, team_path(team, ~p"/machines"))
      assert html_response(conn, 200) =~ machine.name
    end

    test "does not list machines from other teams", %{conn: conn, team: team} do
      other_team = insert(:team)
      insert(:machine, team: other_team)

      conn = get(conn, team_path(team, ~p"/machines"))
      assert html_response(conn, 200) =~ "Empty here, add a machine"

      conn = get(conn, team_path(other_team, ~p"/machines"))
      assert html_response(conn, 302)
    end
  end

  describe "show" do
    setup :register_and_log_in_user
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      {:ok, hypervisor: hypervisor}
    end

    test "shows a machine", %{conn: conn, team: team, hypervisor: hypervisor} do
      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:ok, Enum.random(["Running", "Stopped", "Blocked"])}
      end)
      machine = insert(:machine, hypervisor: hypervisor, team: team, created_at: datetime_now())
      conn = get(conn, team_path(team, ~p"/machines/#{machine}"))
      assert html_response(conn, 200) =~ machine.name
    end

    test "renders access denied when user wants see a machine from other team", %{
      conn: conn,
      team: team
    } do
      other_team = insert(:team)
      machine = insert(:machine, team: other_team)

      conn = get(conn, team_path(team, ~p"/machines/#{machine}"))
      assert html_response(conn, 403)
    end

    test "shows error when update status return error", %{conn: conn, team: team} do
      job = insert(:job, last_status: "blocked")
      machine = insert(:machine, team: team, job_id: job.id)

      conn = get(conn, team_path(team, ~p"/machines/#{machine}"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) ==
               "Machine was not created successfully"
    end
  end

  describe "edit" do
    setup :register_and_log_in_user

    test "does not render delete from when machine is not created", %{conn: conn, team: team} do
      machine = insert(:machine, team: team, last_status: "deleted")

      conn = get(conn, ~p"/machines/#{machine}/edit")
      refute html_response(conn, 200) =~ "Delete machine"
    end

    test "renders delete form when machine is created", %{conn: conn, team: team} do
      machine = insert(:machine, team: team, last_status: "Stopped", created_at: datetime_now())

      conn = get(conn, ~p"/machines/#{machine}/edit")
      assert html_response(conn, 200) =~ "Delete machine"
    end
  end

  describe "ensure_user_has_ssh_private_key plug" do
    setup :register_and_log_in_user
    setup %{conn: conn} do
      hypervisor = insert(:hypervisor)
      {:ok, conn: conn, hypervisor: hypervisor}
    end


    test "redirects to new_ssh_private_key_path when user has no ssh private key", %{conn: conn, team: team, hypervisor: hypervisor} do
      conn = get(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/new"))
      assert redirected_to(conn) == ~p"/users/keys"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "You need to add your SSH public key before creating a machine"
    end

    test "redirects only on new machine creation", %{conn: conn, team: team, hypervisor: hypervisor} do
      conn = get(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/new"))
      assert redirected_to(conn) == ~p"/users/keys"

      conn = get(conn, team_path(team, ~p"/machines"))
      assert html_response(conn, 200) =~ "Listing Machines"
    end

    test "does not redirect when user has at least one ssh public key", %{conn: conn, user: user, team: team, hypervisor: hypervisor} do
      insert(:ssh_public_key, user: user)

      conn = get(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/new"))
      assert html_response(conn, 200) =~ "New Machine"
    end
  end

  describe "new" do
    setup :register_and_log_in_user

    setup %{conn: conn, user: user} do
      hypervisor = insert(:hypervisor)
      insert(:ssh_public_key, user: user)
      {:ok, conn: conn, hypervisor: hypervisor}
    end

    test "renders form", %{conn: conn, team: team, hypervisor: hypervisor} do
      conn = get(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/new"))
      assert html_response(conn, 200) =~ "New Machine"
    end

    test "renders error when hypervisor is not found", %{conn: conn, team: team} do
      conn = get(conn, team_path(team, ~p"/hypervisors/99999999/machines/new"))
      assert html_response(conn, 404)
    end
  end

  describe "create machine" do
    setup :register_and_log_in_user

    setup %{user: user} do
      insert(:ssh_public_key, user: user)
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      network = insert(:network, hypervisor: hypervisor)
      ip_pool(%{network: network})
      {:ok, hypervisor: hypervisor, network: network}
    end

    test "redirects to team machine page when it was created", %{conn: conn, user: user, team: team, hypervisor: hypervisor, network: network} do
      job_id = unique_job_id()
      Webapp.Hypervisors.Mock
      |> expect(:create_machine, fn _repo, _multi_changes, _machine ->
        {:ok, job_id}
      end)

      %{team: another_team} = insert(:team_member, user: user)

      machine_params = %{
        "machine" => %{
          "name" => unique_machine_name(),
          "plan_id" => insert(:plan).id,
          "distribution_id" => insert(:distribution).id,
          "hypervisor_id" => hypervisor.id,
          "team_id" => another_team.id,
          "network_ids" => [network.id]
        }
      }
      conn = post(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/create"), machine_params)

      machine = get_machine_by_job_id(job_id)
      assert redirected_to(conn) == team_path(another_team, ~p"/machines/#{machine}")
    end

    test "renders error when invalid data is sent", %{conn: conn, team: team, hypervisor: hypervisor} do
      machine_params = %{
        "machine" => %{
          "name" => "Invalid machine name",
          "plan_id" => insert(:plan).id,
          "distribution_id" => insert(:distribution).id,
          "hypervisor_id" => hypervisor.id,
          "team_id" => nil,
          "network_ids" => []
        }
      }

      conn = post(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/create"), machine_params)
      assert html_response(conn, 200) =~ "New Machine"
      assert html_response(conn, 200) =~ "Oops, something went wrong! Please check the errors below."
      assert html_response(conn, 200) =~ "Name must only contain letters and numbers and . -"
    end

    test "renders error from hypervisor response", %{conn: conn, team: team, hypervisor: hypervisor, network: network} do
      Webapp.Hypervisors.Mock
      |> expect(:create_machine, fn _repo, _multi_changes, _machine ->
        {:error, "Hypervisor error"}
      end)

      machine_params = %{
        "machine" => %{
          "name" => unique_machine_name(),
          "plan_id" => insert(:plan).id,
          "distribution_id" => insert(:distribution).id,
          "hypervisor_id" => hypervisor.id,
          "team_id" => team.id,
          "network_ids" => [network.id]
        }
      }

      conn = post(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/create"), machine_params)
      assert html_response(conn, 200) =~ "New Machine"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end

    test "renders error when machine cannot be inserted to db", %{conn: conn, team: team, hypervisor: hypervisor, network: network} do
      Webapp.Hypervisors.Mock
      |> expect(:create_machine, fn _repo, _multi_changes, _machine ->
        {:ok, "invalid_job_id"}
      end)

      machine_params = %{
        "machine" => %{
          "name" => unique_machine_name(),
          "plan_id" => insert(:plan).id,
          "distribution_id" => insert(:distribution).id,
          "hypervisor_id" => hypervisor.id,
          "team_id" => team.id,
          "network_ids" => [network.id]
        }
      }

      conn = post(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/create"), machine_params)
      assert html_response(conn, 200) =~ "New Machine"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Couldn't spawn new machine."
    end

    test "renders error when hypervisor is not found", %{conn: conn, team: team, hypervisor: hypervisor, network: network} do
      machine_params = %{
        "machine" => %{
          "name" => unique_machine_name(),
          "plan_id" => insert(:plan).id,
          "distribution_id" => insert(:distribution).id,
          "hypervisor_id" => insert(:hypervisor, hypervisor_type: get_hypervisor_type("unknown")).id,
          "team_id" => team.id,
          "network_ids" => [network.id]
        }
      }

      conn = post(conn, team_path(team, ~p"/hypervisors/#{hypervisor}/machines/create"), machine_params)
      assert html_response(conn, 200) =~ "New Machine"

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Couldn't spawn new machine."
    end
  end

  describe "delete machine" do
    setup :register_and_log_in_user
    setup do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      {:ok, hypervisor: hypervisor}
    end

    test "redirects to team machine page when it was deleted", %{conn: conn, team: team, hypervisor: hypervisor} do
      job_id = unique_job_id()
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())

      Webapp.Hypervisors.Mock
      |> expect(:delete_machine, fn _repo, _multi_changes, _machine ->
        {:ok, job_id}
      end)

      conn = delete(conn, team_path(team, ~p"/machines/#{machine}"))
      assert redirected_to(conn) == team_path(team, ~p"/machines")
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Machine #{machine.name} has been marked for deletion"
    end

    test "renders error when machine cannot be deleted", %{conn: conn, team: team, hypervisor: hypervisor} do
      Webapp.Hypervisors.Mock
      |> expect(:delete_machine, fn _repo, _multi_changes, _machine ->
        {:error, "Hypervisor error"}
      end)

      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      conn = delete(conn, team_path(team, ~p"/machines/#{machine}"))
      assert redirected_to(conn) == team_path(team, ~p"/machines")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end

    test "renders error when hypervisor is not found", %{conn: conn, team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("unknown"))

      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      conn = delete(conn, team_path(team, ~p"/machines/#{machine}"))
      assert redirected_to(conn) == team_path(team, ~p"/machines")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Couldn't delete machine"
    end
  end

  describe "start" do
    setup :register_and_log_in_user
    setup %{team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      {:ok, machine: machine}
    end

    test "redirects to team machine page when it was started", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:start_machine, fn _machine ->
        {:ok, "Starting machine..."}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/start"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Machine #{machine.name} is being started"
    end

    test "renders error when machine cannot be started", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:start_machine, fn _machine ->
        {:error, "Hypervisor error"}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/start"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end
  end

  describe "stop" do
    setup :register_and_log_in_user
    setup %{team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      {:ok, machine: machine}
    end

    test "redirects to team machine page when it was stopped", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:stop_machine, fn _machine ->
        {:ok, "Stopping machine..."}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/stop"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Machine #{machine.name} is being stopped"
    end

    test "renders error when machine cannot be stopped", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:stop_machine, fn _machine ->
        {:error, "Hypervisor error"}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/stop"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end
  end

  describe "restart" do
    setup :register_and_log_in_user
    setup %{team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      {:ok, machine: machine}
    end

    test "redirects to team machine page when it was restarted", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:restart_machine, fn _machine ->
        {:ok, "Restarting machine..."}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/restart"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Machine #{machine.name} is being restarted"
    end

    test "renders error when machine cannot be restarted", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:restart_machine, fn _machine ->
        {:error, "Hypervisor error"}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/restart"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end
  end

  describe "poweroff" do
    setup :register_and_log_in_user
    setup %{team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      {:ok, machine: machine}
    end

    test "redirects to team machine page when it was powered off", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:poweroff_machine, fn _machine ->
        {:ok, "Powering off machine..."}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/poweroff"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
               "Machine #{machine.name} is being powered off"
    end

    test "renders error when machine cannot be powered off", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:poweroff_machine, fn _machine ->
        {:error, "Hypervisor error"}
      end)

      conn = post(conn, team_path(team, ~p"/machines/#{machine}/poweroff"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end
  end

  describe "console" do
    setup :register_and_log_in_user
    setup %{team: team} do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("mock"))
      machine = insert(:machine, team: team, hypervisor: hypervisor, created_at: datetime_now())
      {:ok, machine: machine}
    end

    test "redirects to team machine page when it was powered off", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:console_machine, fn _machine ->
        {:ok, %{"port" => 9999, "user" => "console_user", "password" => "console_password"}}
      end)

      conn = get(conn, team_path(team, ~p"/machines/#{machine}/console"))
      assert conn.assigns.token == Base.encode64("console_user:console_password")
    end

    test "renders error when machine cannot be powered off", %{conn: conn, team: team, machine: machine} do
      Webapp.Hypervisors.Mock
      |> expect(:console_machine, fn _machine ->
        {:error, "Hypervisor error"}
      end)

      conn = get(conn, team_path(team, ~p"/machines/#{machine}/console"))
      assert redirected_to(conn) == team_path(team, ~p"/machines/#{machine}")

      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~
               "Hypervisor error"
    end
  end

  defp get_machine_by_job_id(job_id) do
    job = Webapp.Repo.get_by(Webapp.Jobs.Job, ts_job_id: job_id)
    |> Webapp.Repo.preload(:machine)

    job.machine
  end
end
