defmodule WebappWeb.SSHPublicKeyControllerTest do
  use WebappWeb.ConnCase

  alias Webapp.Accounts

  @create_attrs %{
    ssh_public_key:
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8zjxRY3HmSJ/I6VGvuwEwrQv1hViE9bRx95qz7HR0T7MP10f+X39eKbGF82Nlju1J7u3djgWjNGYp7Yumd3bqbvu8+8Q5yqUYgF2VIDc64Vl0rK4zT6otIKerRcY2vEesZu/pRTZ7VD9dentODLHISRo4+1V+lLEqIFTteBtCxxhJc2g4GVKB2MpL7btZjtqP1X6++8qkg++wXOouNE+3lcgbu6d+SbL9skx3QTO/VwdC+U2yc8lIp2hz79FxUUGIQhV8NuPXp5/sRFGHITkFIu9dxgsqcSoSQQkyBvzd6XCXuwWpAQlmtgsjfYqQdq1/XDL+F2KqH9Vb+rD2dQLT dummy@key",
    title: "some title"
  }
  @invalid_attrs %{ssh_public_key: "invalid public key", title: "some title"}


  setup :register_and_log_in_user
  setup :register_and_log_in_admin

  describe "index" do
    test "lists all ssh_public_keys", %{conn: conn} do
      conn = get(conn, Routes.ssh_public_key_path(conn, :index))
      assert html_response(conn, 200) =~ "SSH Keys"
    end
  end

  describe "new ssh_public_key" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.ssh_public_key_path(conn, :new))
      assert html_response(conn, 200) =~ "Add new SSH public key"
    end
  end

  describe "create ssh_public_key" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.ssh_public_key_path(conn, :create), ssh_public_key: @create_attrs)

      assert redirected_to(conn) == Routes.ssh_public_key_path(conn, :index)
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~
         "Ssh public key created successfully"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.ssh_public_key_path(conn, :create), ssh_public_key: @invalid_attrs)
      assert html_response(conn, 200) =~ "Add new SSH public key"
      assert html_response(conn, 200) =~ "should be at least 80 character(s)"
    end
  end

  describe "delete ssh_public_key" do
    setup [:create_ssh_public_key]

    test "deletes chosen ssh_public_key", %{conn: conn, ssh_public_key: ssh_public_key} do
      assert Accounts.get_ssh_public_key!(ssh_public_key.id) == ssh_public_key

      response = delete(conn, Routes.ssh_public_key_path(conn, :delete, ssh_public_key))
      assert redirected_to(response) == Routes.ssh_public_key_path(conn, :index)

      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_ssh_public_key!(ssh_public_key.id)
      end
    end

    test "cannot delete someone else's key", %{conn: conn, admin: admin} do
      {:ok, ssh_public_key} =  Accounts.create_ssh_public_key(admin, @create_attrs)

      conn = get(conn, Routes.ssh_public_key_path(conn, :show, ssh_public_key))
      assert conn.status == 403

      conn = delete(conn, Routes.ssh_public_key_path(conn, :delete, ssh_public_key))
      assert conn.status == 403
    end
  end

  defp create_ssh_public_key(%{user: user}) do
    {:ok, ssh_public_key} =  Accounts.create_ssh_public_key(user, @create_attrs)
    {:ok, ssh_public_key: ssh_public_key}
  end
end
