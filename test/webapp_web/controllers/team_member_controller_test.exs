defmodule WebappWeb.TeamMemberControllerTest do
  use WebappWeb.ConnCase

  alias Webapp.Teams
  alias Webapp.Accounts
  alias Webapp.Repo

  @create_attrs %{name: "some name", namespace: "some-name"}
  #  @update_attrs %{name: "some updated name"}
  #  @invalid_attrs %{name: nil}

  def fixture(:team) do
    {:ok, team} = Teams.Team.create_changeset(%Teams.Team{}, @create_attrs)
                  |> Repo.insert()
    team
  end

  setup :register_and_log_in_user
  setup :register_and_log_in_admin


  describe "GET /users/teams/:team_id/members/new" do
    setup %{user: user} do
      user = user |> Repo.preload(:teams)
      {:ok, user: user}
    end

    test "renders form", %{conn: conn, team: team} do
      conn = get(conn, ~p"/users/teams/#{team}/members/new")
      assert html_response(conn, 200) =~ "Invite new team member"
    end

    test "responses with access denied for non member user", %{conn: conn, team: user_team} do
      different_team = fixture(:team)
      conn = get(conn, ~p"/users/teams/#{different_team}/members/new")
      assert html_response(conn, 403)

      conn = get(conn, ~p"/users/teams/#{user_team}/members/new")
      assert html_response(conn, 200) =~ "Invite new team member"
    end
  end

  describe "POST /users/teams/:team_id/members" do
    setup %{user: user} do
      user = user |> Repo.preload(:teams)
      {:ok, user: user}
    end

    test "creates a new user account when there is no matching user for given email address", %{conn: conn, team: team} do
      new_user_email = unique_user_email()

      assert Accounts.get_user_by_email(new_user_email) == nil

      conn = post(conn, ~p"/users/teams/#{team}/members", %{
        "invitation" => %{
          "email" => new_user_email,
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Invitation sent to #{new_user_email}."

      new_user = Accounts.get_user_by_email(new_user_email)
      refute new_user == nil
      refute new_user.invited_at == nil

      assert Teams.member_of_team?(new_user, team) == true
    end

    test "generates invitation token for new user account", %{conn: conn, team: team} do
      new_user_email = unique_user_email()

      assert Accounts.get_user_by_email(new_user_email) == nil

      conn = post(conn, ~p"/users/teams/#{team}/members", %{
        "invitation" => %{
          "email" => new_user_email,
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Invitation sent to #{new_user_email}"

      new_user = Accounts.get_user_by_email(new_user_email)
      assert Repo.get_by!(Accounts.UserToken, user_id: new_user.id).context == "invite"
    end

    test "returns error when user is already member of the team", %{conn: conn, user: user, team: team} do
      assert Teams.member_of_team?(user, team) == true

      conn = post(conn, ~p"/users/teams/#{team}/members", %{
        "invitation" => %{
          "email" => user.email,
        }}
      )

      assert html_response(conn, 200) =~ "User is already a member of the team"
    end

    test "does not create a new user account when there is a matching user for given email address", %{conn: conn, team: team} do
      existing_user = user_fixture()
                      |> confirm_user()

      refute Teams.member_of_team?(existing_user, team) == true

      conn = post(conn, ~p"/users/teams/#{team}/members", %{
        "invitation" => %{
          "email" => existing_user.email,
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "User #{existing_user.name} has been added to the team"

      assert Teams.member_of_team?(existing_user, team) == true

      assert_raise Ecto.NoResultsError, fn ->
        Repo.get_by!(Accounts.UserToken, user_id: existing_user.id)
      end
    end


    test "sends invitation email to existing user if it's not confirmed", %{conn: conn, team: team} do
      existing_user = user_fixture()

      refute Teams.member_of_team?(existing_user, team) == true

      conn = post(conn, ~p"/users/teams/#{team}/members", %{
        "invitation" => %{
          "email" => existing_user.email,
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Invitation sent to #{existing_user.email}."

      assert Teams.member_of_team?(existing_user, team) == true

      assert Repo.get_by!(Accounts.UserToken, user_id: existing_user.id).context == "invite"
    end
  end

  describe "DELETE /users/teams/:team_id/members/:id" do
    setup %{user: user, team: team} do
      member = Teams.get_user_team_member(user, team)

      another_user = user_fixture() |> confirm_user()
      {:ok, another_member} = Teams.create_member(%{user_id: another_user.id, team_id: team.id})
      another_conn = build_conn() |> log_in_user(another_user)

    {:ok,
      team: team,
      user: user,
      member: member,
      another_conn: another_conn,
      another_user: another_user,
      another_member: another_member
    }
    end

    test "removes member from the team", %{conn: conn, team: team, another_member: member, another_user: user} do
      assert Teams.member_of_team?(user, team) == true

      conn = delete(conn, ~p"/users/teams/#{team.id}/members/#{member.id}")
      assert redirected_to(conn) == ~p"/users/teams/#{team}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Member #{user.name} deleted successfully."

      assert Teams.member_of_team?(user, team) == false
    end

    test "only members with Administrator role can remove other members", %{another_conn: conn, team: team, user: user, member: member} do
      assert Teams.member_of_team?(user, team) == true

      conn = delete(conn, ~p"/users/teams/#{team.id}/members/#{member.id}")
      assert html_response(conn, 403)

      assert Teams.member_of_team?(user, team) == true
    end

    test "does not remove member from the team if it's the last admin", %{conn: conn, user: user, member: member, another_member: another_member, team: team} do
      Teams.delete_member(another_member)

      team = Teams.get_team!(team.id, [:members])
      assert Enum.count(team.members) == 1

      assert Teams.member_of_team?(user, team) == true

      conn = delete(conn, ~p"/users/teams/#{team.id}/members/#{member.id}")
      assert redirected_to(conn) == ~p"/users/teams/#{team}"
      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~ "Team must have at least one administrator."


      assert Teams.member_of_team?(user, team) == true
    end

    test "returns error when member does not exists", %{conn: conn, team: team} do
      conn = delete(conn, ~p"/users/teams/#{team.id}/members/0")
      assert redirected_to(conn) == ~p"/users/teams/#{team}"
      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~ "Member not found."
    end
  end

  describe "GET /users/teams/:team_id/members/:id/edit" do
    setup %{user: user, team: team} do
      member = Teams.get_user_team_member(user, team)

      another_user = user_fixture() |> confirm_user()
      {:ok, another_member} = Teams.create_member(%{user_id: another_user.id, team_id: team.id})
      another_conn = build_conn() |> log_in_user(another_user)

    {:ok,
      team: team,
      user: user,
      member: member,
      another_conn: another_conn,
      another_user: another_user,
      another_member: another_member
    }
    end

    test "renders form", %{conn: conn, team: team, member: member} do
      conn = get(conn, ~p"/users/teams/#{team.id}/members/#{member.id}/edit")
      assert html_response(conn, 200) =~ "Member Settings"
    end

    test "returns error for non admin user", %{another_conn: conn, team: team, member: member} do
      conn = get(conn, ~p"/users/teams/#{team.id}/members/#{member.id}/edit")
      assert html_response(conn, 403)
    end

    test "returns error when member does not exists", %{conn: conn, team: team} do
      conn = get(conn, ~p"/users/teams/#{team.id}/members/0/edit")
      assert redirected_to(conn) == ~p"/users/teams/#{team}"
      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~ "Member not found."
    end
  end

  describe "PUT /users/teams/:team_id/members/:id" do
    setup %{user: user, team: team} do
      member = Teams.get_user_team_member(user, team)

      another_user = user_fixture() |> confirm_user()
      {:ok, another_member} = Teams.create_member(%{user_id: another_user.id, team_id: team.id})
      another_conn = build_conn() |> log_in_user(another_user)

    {:ok,
      team: team,
      user: user,
      member: member,
      another_conn: another_conn,
      another_user: another_user,
      another_member: another_member
    }
    end

    test "updates member", %{conn: conn, user: user, team: team, member: member} do
      conn = put(conn, ~p"/users/teams/#{team.id}/members/#{member.id}", %{
        "member" => %{
          "role" => "Administrator",
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Member updated successfully."

      member = Teams.get_user_team_member(user, team)
      assert member.role == "Administrator"
    end

    test "returns error for non admin user", %{another_conn: conn, team: team, member: member} do
      conn = put(conn, ~p"/users/teams/#{team.id}/members/#{member.id}", %{
        "member" => %{
          "role" => "Administrator",
        }}
      )

      assert html_response(conn, 403)
    end

    test "returns error when member does not exists", %{conn: conn, team: team} do
      conn = put(conn, ~p"/users/teams/#{team.id}/members/0", %{
        "member" => %{
          "role" => "Administrator",
        }}
      )

      assert redirected_to(conn) == ~p"/users/teams/#{team.id}"
      assert Phoenix.Flash.get(conn.assigns.flash, :error) =~ "Member not found."
    end
  end

end
