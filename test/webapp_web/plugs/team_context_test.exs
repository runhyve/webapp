defmodule WebappWeb.TeamContextTest do
  use WebappWeb.ConnCase

  alias Webapp.{
    Teams,
    Accounts
  }

  setup %{conn: conn} do
    # create user
    user = user_fixture(%{user_email: "user#{System.unique_integer()}@example.com"})
    |> confirm_user()

    another_user = user_fixture(%{user_email: "user#{System.unique_integer()}@example.com"})
    |> confirm_user()

    # create team
    {:ok, team} =
      Teams.create_team(%{
        name: "some name",
        namespace: "user-company",
        members: [%{"user_id" => user.id, "role" => "Administrator"}]
      })

    {:ok, _another_team} =
      Teams.create_team(%{
        name: "other name",
        namespace: "other-company",
        members: [%{"user_id" => another_user.id, "role" => "Administrator"}]
      })

    {:ok, %{conn: log_in_user(conn, user), team: team, user: user}}
  end

  test "first user team is assigned as current_team if namespace is not provided in url", %{
    conn: conn,
    user: user
  } do
    conn = get(conn, "/")
    team = Accounts.user_first_team(user)
    assert conn.assigns[:current_team] == team
  end

  test "current_team is taken form url", %{conn: conn, user: _user, team: team} do
    conn = get(conn, "/user-company/machines")
    # current_team is loaded without members
    team = Teams.get_team!(team.id)
    assert conn.assigns[:current_team] == team
  end

  test "user can not access other teams", %{conn: conn, user: _user, team: _team} do
    conn = get(conn, "/other-company/machines")

    assert conn.halted
    assert conn.status == 302
    assert redirected_to(conn) == "/machines"
  end
end
