defmodule WebappWeb.TeamPrefixTest do
  use WebappWeb.ConnCase

  alias Webapp.Teams

  setup %{conn: conn} do
    # create user
    user = user_fixture(%{user_email: "user#{System.unique_integer()}@example.com"})
    |> confirm_user()

    another_user = user_fixture(%{user_email: "user#{System.unique_integer()}@example.com"})
    |> confirm_user()

    # create team
    {:ok, team} =
      Teams.create_team(%{
        name: "some name",
        namespace: "user-company",
        members: [%{"user_id" => user.id, "role" => "Administrator"}]
      })

    {:ok, _another_team} =
      Teams.create_team(%{
        name: "other name",
        namespace: "other-company",
        members: [%{"user_id" => another_user.id, "role" => "Administrator"}]
      })

    {:ok, %{conn: log_in_user(conn, user), team: team, user: user}}
  end

  test "assigns user team as current_team form url", %{conn: conn, user: _user, team: team} do
    conn = get(conn, "/user-company/machines")
    # current_team is loaded without members
    team = Teams.get_team!(team.id)
    assert conn.assigns[:current_team] == team
  end

  test "team namespace is removed from path_info", %{conn: conn} do
    conn = get(conn, "/user-company/machines")
    assert conn.path_info == ["machines"]
  end

  test "restricted namespaces are not removed from path_info", %{conn: conn} do
    conn = get(conn, "/health")
    assert conn.path_info == ["health"]
  end

  test "responses with not found if team not exists", %{conn: conn} do
    conn = get(conn, "/non-existing-company/machines")
    assert conn.status == 404
  end
end
