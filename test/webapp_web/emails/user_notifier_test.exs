defmodule WebappWeb.Emails.UserNotifierlTest do
  use WebappWeb.ConnCase

  import Swoosh.TestAssertions

  alias WebappWeb.Emails.UserNotifier

  setup :set_swoosh_global

  setup do
    user = Webapp.AccountsFixtures.user_fixture()
    {:ok, %{user: user}}
  end

  test "sends confirmation instructions", %{user: user} do
    {:ok, sent_email} = UserNotifier.deliver_confirmation_instructions(user, "confirmation_url")

    assert sent_email.subject =~ "Confirmation instructions"
    assert sent_email.text_body =~ "confirmation_url"
    assert_email_sent(sent_email)
  end

  test "sends reset password instructions", %{user: user} do
    {:ok, sent_email} = UserNotifier.deliver_reset_password_instructions(user, "reset_password_url")

    assert sent_email.subject =~ "Reset password instructions"
    assert sent_email.text_body =~ "reset_password_url"
    assert_email_sent(sent_email)
  end
end
