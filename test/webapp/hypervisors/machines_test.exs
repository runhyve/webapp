defmodule Webapp.MachinesTest do
  use Webapp.DataCase
  import Mox

  alias Webapp.Machines
  alias Webapp.Networks

  setup :verify_on_exit!

  describe "create_machine/1" do
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("Mock"))
      team = insert(:team)
      network = insert(:network, hypervisor: hypervisor)
      ip_pool = ip_pool(%{ip_range: "192.168.88.0/24", network: network})
      distribution = insert(:distribution)
      plan = insert(:plan)

      {:ok,
        hypervisor: hypervisor,
        team: team,
        network: network,
        ip_pool: ip_pool,
        distribution: distribution,
        plan: plan
      }
    end

    test "with valid data creates machine assigns first free ipv4 from the pool and creates job to track progress on hypervisor", assigns do
      ipv4 = Networks.get_unused_ipv4(assigns.network)
      job_id = unique_job_id()


      attrs = %{
        "name" => "test",
        "hypervisor_id" => assigns.hypervisor.id,
        "network_ids" => [assigns.network.id],
        "team_id" => assigns.team.id,
        "distribution_id" => assigns.distribution.id,
        "plan_id" => assigns.plan.id,
      }

      Webapp.Hypervisors.Mock
      |> expect(:create_machine, fn _repo, _multi_changes, _machine ->
        {:ok, job_id}
      end)

      # IP is free before creating machine
      assert ipv4.machine_id == nil
      assert ipv4.reserved == false

      assert {:ok, %{machine: machine}} = Machines.create_machine(attrs)

      job = Webapp.Repo.get_by!(Webapp.Jobs.Job, ts_job_id: job_id)
      assert machine.name == "test"
      assert machine.job_id == job.id

      assert machine.last_status == "Creating"

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)

      assert ipv4.machine_id == machine.id
      assert ipv4.reserved == false
    end

    test "returns error when data is invalid", assigns do
      attrs = %{
        "name" => "test",
        "hypervisor_id" => assigns.hypervisor.id,
        "network_ids" => [assigns.network.id],
        "team_id" => assigns.team.id,
      }

      assert {:error, changeset} = Machines.create_machine(attrs)

      assert %{
        distribution_id: ["can't be blank"],
        plan_id: ["can't be blank"]
      } = errors_on(changeset)
    end

    test "returns error when there is no free ip address in pool", assigns do
      network = insert(:network, hypervisor: assigns.hypervisor)
      ip_pool = ip_pool(%{ip_range: "192.168.88.252/30", network: network})
      Enum.each(ip_pool.ipv4, &Networks.reserve_ipv4(&1))

      attrs = %{
        "name" => "test",
        "hypervisor_id" => assigns.hypervisor.id,
        "network_ids" => [network.id],
        "team_id" => assigns.team.id,
        "distribution_id" => assigns.distribution.id,
        "plan_id" => assigns.plan.id,
      }

      assert {:error, changeset} = Machines.create_machine(attrs)

      assert %{
        ipv4: ["should have at least 1 item(s)"]
      } = errors_on(changeset)
    end
  end

  describe "delete_machine/1" do
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("Mock"))
      network = insert(:network, hypervisor: hypervisor)
      ip_pool = ip_pool(%{ip_range: "192.168.88.0/24", network: network})
      {:ok, hypervisor: hypervisor, network: network, ip_pool: ip_pool}
    end

    test "marks machine as deleted, releases ipv4 address and silently ties to remove it form hypervisor", %{hypervisor: hypervisor, network: network} do
      ipv4 = Networks.get_unused_ipv4(network)
      machine = insert(:machine, hypervisor: hypervisor, last_status: "Failed", failed_at: datetime_now(), ipv4: [ipv4])

      assert is_nil(machine.deleted_at)

      Webapp.Hypervisors.Mock
      |> expect(:delete_machine, fn _machine ->
        {:error, "It should be ignored"}
      end)

      assert {:ok, %{machine: deleted_machine}} = Machines.delete_machine(machine)

      assert deleted_machine.last_status == "Deleted"
      assert deleted_machine.deleted_at != nil
      assert deleted_machine.name =~ "deleted_"

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)
      assert ipv4.machine_id == nil
      assert ipv4.reserved == false
    end

    test "marks machine as deleted and release ipv4 address when it does not exists on hypervisor", %{hypervisor: hypervisor, network: network} do
      ipv4 = Networks.get_unused_ipv4(network)
      machine = insert(:machine, hypervisor: hypervisor, last_status: "Running", job_id: nil, created_at: datetime_now(), ipv4: [ipv4])

      assert is_nil(machine.deleted_at)

      Webapp.Hypervisors.Mock
      |> expect(:delete_machine, fn _repo, _multi_changes, _machine ->
        {:ok, nil}
      end)

      assert {:ok, %{machine: deleted_machine}} = Machines.delete_machine(machine)

      assert deleted_machine.last_status == "Deleted"
      assert deleted_machine.deleted_at != nil
      assert deleted_machine.name =~ "deleted_"

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)
      assert ipv4.machine_id == nil
      assert ipv4.reserved == false
    end

    test "marks machine as deleted, creates job for tracking progress on hypervisor and does not releases ipv4 address yet", %{hypervisor: hypervisor, network: network} do
      ipv4 = Networks.get_unused_ipv4(network)
      machine = insert(:machine, hypervisor: hypervisor, last_status: "Running", job_id: nil, created_at: datetime_now(), ipv4: [ipv4])
      job_id = unique_job_id()

      Webapp.Hypervisors.Mock
      |> expect(:delete_machine, fn _repo, _multi_changes, _machine ->
        {:ok, job_id}
      end)

      assert {:ok, %{machine: deleted_machine}} = Machines.delete_machine(machine)

      job = Webapp.Repo.get_by!(Webapp.Jobs.Job, ts_job_id: job_id)
      assert deleted_machine.job_id == job.id
      assert deleted_machine.last_status == "Deleting"
      assert deleted_machine.deleted_at == nil

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)
      assert ipv4.machine_id == deleted_machine.id
    end
  end

  describe "check_machine_status/1" do
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("Mock"))
      {:ok, hypervisor: hypervisor}
    end

    test "checks machine status on hypervisor", %{hypervisor: hypervisor} do
      machine = insert(:machine, hypervisor: hypervisor, last_status: "Creating")

      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:ok, "Updated Status"}
      end)

      assert {:ok, updated_machine} = Machines.check_machine_status(machine)
      assert updated_machine.last_status == "Updated Status"
      refute machine == updated_machine
    end

    test "marks machine as failed when it does not exists on hypervisor", %{hypervisor: hypervisor} do
      machine = insert(:machine, hypervisor: hypervisor, job: build(:job), last_status: "Creating")
      assert machine.job_id != nil

      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:error, "Virtual machine #{machine.uuid} doesn't exist"}
      end)

      assert {:error, error} = Machines.check_machine_status(machine)
      assert error =~ "Machine does not exist on hypervisor"

      updated_machine = Machines.get_machine!(machine.id)
      assert updated_machine.last_status == "Failed"
      assert updated_machine.failed_at != nil
      assert updated_machine.job_id == nil
    end

    test "returns error when machine status check fails", %{hypervisor: hypervisor} do
      machine = insert(:machine, hypervisor: hypervisor, last_status: "Creating")

      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:error, "Something went wrong"}
      end)

      assert {:error, error} = Machines.check_machine_status(machine)
      assert error == "Something went wrong"
    end

    test "returns error when hypervisor was not found" do
      hyervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("unknown"))
      machine = insert(:machine, hypervisor: hyervisor, last_status: "Creating")

      assert {:error, :hypervisor_not_found} = Machines.check_machine_status(machine)
    end
  end

  describe "update_status/1" do
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("Mock"))
      {:ok, hypervisor: hypervisor}
    end

    test "does not check status for failed machine" do
      machine = insert(:machine, created_at: datetime_now(), failed_at: datetime_now())

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert machine == updated_machine
    end

    test "does not check status for deleted machine" do
      machine = insert(:machine, created_at: datetime_now(), deleted_at: datetime_now())

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert machine == updated_machine
    end

    test "checks machine status on hypervisor when job is empty", %{hypervisor: hypervisor} do
      machine = insert(:machine, job: nil, created_at: datetime_now(), hypervisor: hypervisor, last_status: "Creating")

      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:ok, "Updated Status"}
      end)

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert updated_machine.last_status == "Updated Status"
      refute machine == updated_machine
    end

    test "waits for the job to start when job is queued", %{hypervisor: hypervisor} do
      job = insert(:job, last_status: "queued", started_at: datetime_now())
      machine = insert(:machine, job: job, created_at: datetime_now(), hypervisor: hypervisor, last_status: "Creating")

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert machine == updated_machine
      assert is_nil(updated_machine.failed_at)
    end

    test "waits for job to finish when job is running", %{hypervisor: hypervisor} do
      job = insert(:job, last_status: "running", started_at: datetime_now())
      machine = insert(:machine, job: job, created_at: datetime_now(), hypervisor: hypervisor, last_status: "Creating")

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert machine == updated_machine
      assert is_nil(updated_machine.failed_at)
    end

    test "when job is running for too long, marks machine as failed" do
      job = insert(:job, last_status: "running", started_at: ~U[2021-01-12 00:01:00.00Z])
      machine = insert(:machine, job: job, created_at: datetime_now(), last_status: "Creating")

      assert is_nil(machine.failed_at)
      assert {:error, error} = Machines.update_status(machine)
      assert error =~ "Something went wrong, creating virtual machine took too long"

      updated_machine = Machines.get_machine!(machine.id)

      assert updated_machine.last_status == "Failed"
      refute is_nil(updated_machine.failed_at)
    end

    test "marks machine as failed when job is blocked" do
      job = insert(:job, last_status: "blocked", started_at: datetime_now())
      machine = insert(:machine, job: job, created_at: datetime_now(), last_status: "Creating")

      assert is_nil(machine.failed_at)
      assert {:error, error} = Machines.update_status(machine)
      assert error =~ "Machine was not created successfully"

      updated_machine = Machines.get_machine!(machine.id)

      assert updated_machine.last_status == "Failed"
      refute is_nil(updated_machine.failed_at)
    end

    test "marks machine as failed when job is failed" do
      job = insert(:job, last_status: "failed", started_at: datetime_now())
      machine = insert(:machine, job: job, created_at: datetime_now(), last_status: "Creating")

      assert is_nil(machine.failed_at)
      assert {:error, error} = Machines.update_status(machine)
      assert error =~ "Machine was not created successfully"

      updated_machine = Machines.get_machine!(machine.id)

      assert updated_machine.last_status == "Failed"
      refute is_nil(updated_machine.failed_at)
    end

    test "marks machine as created when create job is finished", %{hypervisor: hypervisor} do
      job = insert(:job, last_status: "finished", e_level: 0, started_at: datetime_now())
      machine = insert(:machine, job: job, last_status: "Creating", hypervisor: hypervisor)

      Webapp.Hypervisors.Mock
      |> expect(:update_machine_status, fn _repo, _multi_changes, _machine ->
        {:ok, "Created"}
      end)

      assert is_nil(machine.created_at)
      refute is_nil(machine.job_id)

      assert {:ok, updated_machine} = Machines.update_status(machine)
      assert updated_machine.last_status == "Created"
      refute machine == updated_machine

      refute is_nil(updated_machine.created_at)
      assert is_nil(updated_machine.job_id)
    end

    test "marks machine as failed when create job failed" do
      job = insert(:job, last_status: "finished", e_level: 1, started_at: datetime_now())
      machine = insert(:machine, job: job, last_status: "Creating")

      assert is_nil(machine.failed_at)
      assert {:error, error} = Machines.update_status(machine)
      assert error =~ "Machine was not created successfully"

      updated_machine = Machines.get_machine!(machine.id)

      assert updated_machine.last_status == "Failed"
      refute is_nil(updated_machine.failed_at)
    end

    test "marks machine as deleted and releases ip when delete job is finished", %{hypervisor: hypervisor} do
      job = insert(:job, last_status: "finished", e_level: 0, started_at: datetime_now())
      network = insert(:network, hypervisor: hypervisor)
      ip_pool = ip_pool(%{ip_range: "192.168.99.0/24", network: network})
      ipv4 = hd(ip_pool.ipv4)

      machine = insert(:machine, job: job, ipv4: [ipv4], last_status: "Deleting", hypervisor: hypervisor)
      assert is_nil(machine.deleted_at)
      refute machine.ipv4 == []

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)
      assert ipv4.machine_id == machine.id

      assert {:ok, _machine} = Machines.update_status(machine)

      updated_machine = Machines.get_machine!(machine.id, [:ipv4])
      assert updated_machine.last_status == "Deleted"

      assert updated_machine.name =~ "deleted_"
      refute is_nil(updated_machine.deleted_at)
      assert updated_machine.ipv4 == []

      ipv4 = Webapp.Repo.get!(Webapp.Networks.Ipv4, ipv4.id)
      assert is_nil(ipv4.machine_id)
    end

    test "marks machine as failed when delete job failed" do
      job = insert(:job, last_status: "finished", e_level: 1, started_at: datetime_now())
      machine = insert(:machine, job: job, last_status: "Deleting")

      assert is_nil(machine.failed_at)
      assert {:error, error} = Machines.update_status(machine)
      assert error =~ "Machine was not deleted successfully"

      updated_machine = Machines.get_machine!(machine.id)

      assert updated_machine.last_status == "Failed"
      refute is_nil(updated_machine.failed_at)
    end
  end

  describe "get_machine_hid/1" do
    test "returns name for given machine when uuid is not available" do
      machine =
        insert(:machine)
        |> Webapp.Machines.Machine.update_changeset(%{})
        |> Ecto.Changeset.put_change(:uuid, nil)
        |> Repo.update!()

      assert machine.uuid == nil
      assert "#{machine.team_id}_#{machine.name}" == Machines.get_machine_hid(machine)
    end

    test "returns uuid when is available" do
      machine = insert(:machine)

      assert machine.uuid == Machines.get_machine_hid(machine)
    end
  end

  describe "machine_can_do?/2" do
    test "returns false when machine is marked as failed for all except :delete" do
      machine = insert(:machine, created_at: datetime_now(), failed_at: datetime_now())

      allowed_actions = [:delete]
      for action <- allowed_actions do
        assert Machines.machine_can_do?(machine, action)
      end

      forbidden_actions = [:start, :stop, :reboot, :poweroff, :console]
      for action <- forbidden_actions do
        refute Machines.machine_can_do?(machine, action)
      end
    end

    test "returns false for all when machine is marked as deleted" do
      machine = insert(:machine, created_at: datetime_now(), deleted_at: datetime_now())

      actions = [:start, :stop, :reboot, :poweroff, :console, :delete]
      for action <- actions do
        refute Machines.machine_can_do?(machine, action)
      end
    end

    test "returns true for :console when machine is running or in bootloader state" do
      allowed_states = ["Running", "Bootloader"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :console)
      end

      forbidden_states = ["Creating", "Deleting", "Stopped", "Failed", "Deleted"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :console)
      end
    end

    test "returns true for :start when machine state is not in running, bootloaded, creating or deleting state" do
      allowed_states = ["Stopped", "Locked"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :start)
      end

      forbidden_states = ["Running", "Bootloader", "Creating", "Deleting"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :start)
      end
    end

    test "returns true for :stop when machine state is in running or bootloader state" do
      allowed_states = ["Running", "Bootloader"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :stop)
      end

      forbidden_states = ["Creating", "Deleting", "Stopped", "Failed", "Deleted"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :stop)
      end
    end

    test "returns true for :restart when machine state is in running or bootloader state" do
      allowed_states = ["Running", "Bootloader"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :restart)
      end

      forbidden_states = ["Creating", "Deleting", "Stopped", "Failed", "Deleted"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :restart)
      end
    end

    test "returns true for :poweroff when machine state is not in creating, deleting or stopped state" do
      allowed_states = ["Running", "Bootloader"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :poweroff)
      end

      forbidden_states = ["Stopped", "Creating", "Deleting"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :poweroff)
      end
    end

    test "returns true for :delete when machine state is not in creating or deleting state" do
      allowed_states = ["Running", "Bootloader", "Stopped"]
      for state <- allowed_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        assert Machines.machine_can_do?(machine, :delete)
      end

      forbidden_states = ["Creating", "Deleting"]
      for state <- forbidden_states do
        machine = insert(:machine, created_at: datetime_now(), last_status: state)
        refute Machines.machine_can_do?(machine, :delete)
      end
    end
  end
end
