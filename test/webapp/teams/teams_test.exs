defmodule Webapp.TeamsTest do
  use Webapp.DataCase

  alias Webapp.Teams
  alias Webapp.Accounts
  alias Webapp.Teams.{Team, Member}

  @valid_team %{name: "some name", namespace: "another-company"}
  @invalid_team %{name: nil}

  @update_team %{name: "some updated name"}

  def team_fixture(_attrs \\ %{}) do
    {:ok, fixtures} =
      Accounts.register_user(%{
        user_email: "fred@example.com",
        user_password: "reallyHard2gue$$",
        user_name: "fred",
        team_name: "company",
        team_namespace: "company"
      })

    fixtures
  end

  describe "teams" do
    test "list_teams/0 returns all teams" do
      %{team: team} = team_fixture()
      assert Teams.list_teams() == [team]
    end

    test "get_team!/1 returns the team with given id" do
      %{team: team} = team_fixture()
      assert Teams.get_team!(team.id) == team
    end

    test "create_team/1 with valid data creates a team" do
      %{team: _team, user: user} = team_fixture()

      team_params = Map.merge(@valid_team, %{members: [%{user_id: user.id, role: "Administrator"}]})

      assert {:ok, %Team{} = team} = Teams.create_team(team_params)
      assert team.name == "some name"
    end

    test "create_team/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Teams.create_team(@invalid_team)
    end

    test "update_team/2 with valid data updates the team" do
      %{team: team, user: _user} = team_fixture()
      assert {:ok, %Team{} = team} = Teams.update_team(team, @update_team)
      assert team.name == "some updated name"
    end

    test "update_team/2 with invalid data returns error changeset" do
      %{team: team, user: _user} = team_fixture()
      assert {:error, %Ecto.Changeset{}} = Teams.update_team(team, @invalid_team)
      assert team == Teams.get_team!(team.id)
    end

     test "delete_team/1 deletes the team" do
       %{team: team} = team_fixture()
       assert {:ok, %Team{}} = Teams.delete_team(team)
       assert_raise Ecto.NoResultsError, fn -> Teams.get_team!(team.id) end
     end

    test "change_team/1 returns a team changeset" do
      %{team: team, user: _user} = team_fixture()
      assert %Ecto.Changeset{} = Teams.change_team(team)
    end
  end

  describe "members" do
    test "create_member/1 with valid data creates a member" do
      %{user: user} = team_fixture()
      new_team = fixture(:team)

      assert {:ok, %Member{}} = Teams.create_member(
        %{
          user_id: user.id,
          role: "Administrator",
          team_id: new_team.id
        })
    end

    test "create_member/1 returns changeset with error when user is already member" do
      %{team: team, user: user} = team_fixture()

      {:error, changeset} = Teams.create_member(
        %{
          user_id: user.id,
          role: "Administrator",
          team_id: team.id
        })

        assert %{team_id: ["User is already a member of the team"]} =  errors_on(changeset)
    end

    test "create_member/1 with invalid data returns error changeset" do
      %{team: team, user: user} = team_fixture()

      assert {:error, %Ecto.Changeset{}} = Teams.create_member(
        %{
          user_id: user.id,
          role: "Invalid",
          team_id: team.id
        })
    end

    test "invite_member/2 with valid data invites a member" do
      %{team: team} = team_fixture()
      email = unique_user_email()
      assert {:ok, user} = Teams.invite_member(email, team)

      assert Teams.get_user_team_member(user, team).user_id == user.id
    end

    test "invite_member/2 with invalid data returns error changeset" do
      %{team: team} = team_fixture()
      email = "invalid email"
      assert {:error, %Ecto.Changeset{} = changeset} = Teams.invite_member(email, team)

      assert %{email: ["must have the @ sign and no spaces"]} =  errors_on(changeset)
    end

    test "delete_member/1 deletes the member" do
      %{team: team, user: user} = team_fixture()
      member = Teams.get_user_team_member(user, team)

      assert {:ok, %Member{}} = Teams.delete_member(member)
      assert is_nil(Teams.get_user_team_member(user, team))
    end

    test "change_member/1 returns a member changeset" do
      %{team: team, user: user} = team_fixture()
      member = Teams.get_user_team_member(user, team)

      assert %Ecto.Changeset{} = Teams.change_member(member)
    end

    test "update_member/2 with valid data updates the member" do
      %{team: team, user: user} = team_fixture()
      member = Teams.get_user_team_member(user, team)

      assert {:ok, %Member{} = member} = Teams.update_member(member, %{role: "Administrator"})
      assert member.role == "Administrator"
    end

    test "update_member/2 with invalid data returns error changeset" do
      %{team: team, user: user} = team_fixture()
      member = Teams.get_user_team_member(user, team)

      assert {:error, %Ecto.Changeset{}} = Teams.update_member(member, %{role: "Invalid"})
      assert member == Teams.get_user_team_member(user, team)
    end
  end

  defp fixture(:team, attrs \\ @valid_team) do
    %Team{}
    |> Team.create_changeset(attrs)
    |> Repo.insert!()
  end

end
