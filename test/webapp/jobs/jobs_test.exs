defmodule Webapp.JobsTest do
  use Webapp.DataCase
  import Mox

  alias Webapp.Jobs
  alias Webapp.Jobs.Job

  setup :verify_on_exit!

  describe "list" do
    setup _ do
      insert_list(10, :job, last_status: "finished")
      insert_list(10, :job, last_status: "failed")
      insert_list(10, :job, last_status: "running")

      :ok
    end

    test "list_active_jobs/1 returns only active jobs" do
      assert Jobs.list_active_jobs() |> length() == 10
    end

    test "list_jobs/1 returns all jobs" do
      assert Jobs.list_jobs() |> length() == 30
    end
  end

  describe "get" do
    test "get_job!/1 returns job with given id" do
      job = insert(:job)
      assert Jobs.get_job!(job.id) == job

      assert_raise Ecto.NoResultsError, fn -> Jobs.get_job!(job.id + 1) end
    end
  end

  describe "changesets" do
    test "queued_changeset/1 sets last_status to queued" do
      job = insert(:job, last_status: nil)
      changeset = Job.queued_changeset(job)
      assert changeset.changes.last_status == "queued"
    end

    test "running_changeset/1 sets started_at only when it's not set" do
      job = insert(:job, started_at: nil, last_status: "queued")
      changeset = Job.running_changeset(job)
      assert changeset.changes.started_at != nil
      assert changeset.changes.last_status == "running"
    end

    test "running_changeset/1 does not update started_at when it's already set" do
      started_at = ~U[2020-01-01 00:00:00Z]
      job = insert(:job, started_at: started_at, last_status: "queued")
      changeset = Job.running_changeset(job)
      assert Map.get(changeset.changes, started_at, nil) == nil
      assert changeset.changes.last_status == "running"
    end

    test "update_changeset/2 updates last_status and e_level" do
      job = insert(:job, last_status: "queued", e_level: nil)
      changeset = Job.update_changeset(job, %{"last_status" => "running", "e_level" => 2})
      assert changeset.changes.last_status == "running"
      assert changeset.changes.e_level == 2
    end

    test "finished_changeset/2 updates last_status and finished_at" do
      job = insert(:job, last_status: "running", finished_at: nil, e_level: nil)
      changeset = Job.finished_changeset(job, %{"e_level" => 2})
      assert changeset.changes.last_status == "finished"
      assert changeset.changes.e_level == 2
      assert changeset.changes.finished_at != nil
    end

    test "failed_changeset/2 sets last_status to failed" do
      job = insert(:job, last_status: "running", started_at: DateTime.utc_now())
      changeset = Job.failed_changeset(job, %{"e_level" => 2})
      assert changeset.changes.last_status == "failed"
    end
  end

  test "delete/1 deletes job" do
    job = insert(:job)
    assert {:ok, _} = Jobs.delete_job(job)
    assert_raise Ecto.NoResultsError, fn -> Jobs.get_job!(job.id) end
  end

  describe "update_status/1" do
    setup _ do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("Mock"))
      {:ok, hypervisor: hypervisor}
    end

    test "marks job as queued when task is queued on hypervisor", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: nil)

      Webapp.Hypervisors.Mock
      |> expect(:job_status, fn _repo, _multi_changes, _hypervisor, _task_id ->
        {:ok, %{"state" => "queued", "log" => "(file)"}}
      end)

      assert {:ok, updated_job} = Jobs.update_status(job)

      refute job.last_status == "queued"
      assert updated_job.last_status == "queued"
    end

    test "marks job as running and set started_at when task is running on hypervisor", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: "queued")

      Webapp.Hypervisors.Mock
      |> expect(:job_status, fn _repo, _multi_changes, _hypervisor, _task_id ->
        {:ok, %{"state" => "running", "log" => "(file)"}}
      end)

      assert {:ok, updated_job} = Jobs.update_status(job)

      refute job.last_status == "running"
      assert is_nil(job.started_at)
      assert updated_job.last_status == "running"
      assert updated_job.started_at != nil
    end

    test "marks job as finished with proper e_level when task is finished on hypervisor", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: "running")

      Webapp.Hypervisors.Mock
      |> expect(:job_status, fn _repo, _multi_changes, _hypervisor, _task_id ->
        {:ok, %{"state" => "finished", "elevel" => 2}}
      end)

      assert {:ok, updated_job} = Jobs.update_status(job)

      refute job.last_status == "finished"
      assert updated_job.last_status == "finished"
      assert updated_job.e_level == 2
    end

    test "update job state when task state is different from queued, running and finished", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: "running")

      Webapp.Hypervisors.Mock
      |> expect(:job_status, fn _repo, _multi_changes, _hypervisor, _task_id ->
        {:ok, %{"state" => "unknown", "log" => "(file)"}}
      end)

      assert {:ok, updated_job} = Jobs.update_status(job)

      assert job.last_status == "running"
      assert updated_job.last_status == "unknown"
    end

    test "sets job as failed when task does not exists on hypervisor", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: "running")

      Webapp.Hypervisors.Mock
      |> expect(:job_status, fn _repo, _multi_changes, _hypervisor, task_id ->
        {:error, "Couldn't get task with id #{task_id}."}
      end)

      assert {:error, "Job does not exists on hypervisor"} = Jobs.update_status(job)

      updated_job = Repo.get(Job, job.id)
      assert updated_job.last_status == "failed"
      assert updated_job.e_level == 127
    end

    test "returns job when it is already finished", %{hypervisor: hypervisor} do
      job = insert(:job, hypervisor: hypervisor, last_status: "finished", finished_at: DateTime.utc_now())
      assert {:ok, same_job} = Jobs.update_status(job)
      assert job == same_job
    end

    test "returns error when hyperivsor is not found" do
      hypervisor = insert(:hypervisor, hypervisor_type: get_hypervisor_type("unknown"))
      job = insert(:job, hypervisor: hypervisor, last_status: "running")
      assert {:error, :hypervisor_not_found} = Jobs.update_status(job)
    end

    test "remove job when it is failed or finished and finished more than 7 days ago", %{hypervisor: hypervisor} do
      finished_at = DateTime.utc_now() |> DateTime.add(-8, :day)

      job = insert(:job, hypervisor: hypervisor, last_status: "finished", finished_at: finished_at)
      assert {:ok, deleted_job} = Jobs.update_status(job)

      assert_raise Ecto.NoResultsError, fn -> Jobs.get_job!(deleted_job.id) end

      job = insert(:job, hypervisor: hypervisor, last_status: "failed", finished_at: finished_at)
      assert {:ok, deleted_job} = Jobs.update_status(job)

      assert_raise Ecto.NoResultsError, fn -> Jobs.get_job!(deleted_job.id) end
    end
  end
end
