defmodule Webapp.Factory do
  use ExMachina.Ecto, repo: Webapp.Repo

  import Webapp.DataHelpers, only: [datetime_now: 0]

  import Bitwise

  @password Argon2.hash_pwd_salt("password")

  def unique_machine_name, do: "machine-#{System.unique_integer()}"
  def unique_job_id, do: sequence(:ts_job_id, &"#{&1}") |> String.to_integer()

  def user_factory() do
    %Webapp.Accounts.User{
      email: Faker.Internet.email(),
      name: Faker.Internet.user_name(),
      password_hash: @password,
      confirmed_at: datetime_now(),
      role: "User"
    }
  end

  def team_factory() do
    name = Faker.Team.name() <> " #{System.unique_integer()}"

    %Webapp.Teams.Team{
      name: name,
      namespace: Faker.Internet.slug([name])
    }
  end

  def team_member_factory() do
    %Webapp.Teams.Member{
      role: Enum.random(["User", "Administrator"]),
      team: build(:team),
      user: build(:user)
    }
  end

  def region_factory do
    %Webapp.Regions.Region{
      name: Faker.Address.country() <> " #{System.unique_integer()}"
    }
  end

  def hypervisor_factory do
    %Webapp.Hypervisors.Hypervisor{
      name: Faker.Internet.domain_word() <> " #{System.unique_integer()}",
      fqdn: Faker.Internet.domain_name(),
      region: build(:region),
      hypervisor_type: get_hypervisor_type("bhyve")
    }
  end

  def plan_factory do
    %Webapp.Plans.Plan{
      cpu: :rand.uniform(64),
      name: Faker.Superhero.name() <> " #{System.unique_integer()}",
      ram: (1024 * :math.pow(2, :rand.uniform(16))) |> round(),
      storage: 1024 * :rand.uniform(1024),
      price: :rand.uniform(99)
    }
  end

  def distribution_factory do
    %Webapp.Distributions.Distribution{
      image: Faker.Internet.url(),
      loader: Enum.random(["grub", "bhyveload", "uefi-csm"]),
      name: Faker.App.name(),
      version: Faker.App.semver()
    }
  end

  def machine_factory() do
    %Webapp.Machines.Machine{
      name: Faker.App.name(),
      distribution: build(:distribution),
      hypervisor: build(:hypervisor),
      plan: build(:plan),
      team: build(:team)
    }
  end

  def job_factory() do
    %Webapp.Jobs.Job{
      ts_job_id: sequence(:ts_job_id, &"#{&1}"),
      last_status: Enum.random(["pending", "running", "failed", "completed"]),
      e_level: Enum.random([0, 1, 2]),
    }
  end

  def get_hypervisor_type(type) do
    case Webapp.Repo.get_by(Webapp.Hypervisors.Type, name: type) do
      %Webapp.Hypervisors.Type{} = hypervisor_type -> hypervisor_type
      nil -> Webapp.Repo.insert!(%Webapp.Hypervisors.Type{name: type})
    end
  end

  def ssh_public_key_factory() do
    {ssh_public_key, fingerprint} = generate_ssh_public_key()

    %Webapp.Accounts.SSHPublicKey{
      title: Faker.Person.name(),
      ssh_public_key: ssh_public_key,
      fingerprint: fingerprint,
    }
  end

  def network_factory() do
    %Webapp.Networks.Network{
      name: Faker.Internet.domain_word(),
      network: Faker.Internet.ip_v4_address(),
      hypervisor: build(:hypervisor),
    }
  end

  def ip_pool(attrs \\ %{}) do
    cidr = Map.get(attrs, :ip_range, "192.168.1.0/24")
    attrs = Map.drop(attrs, [:ip_range])

    {first, _last, mask} = InetCidr.parse_cidr!(cidr, true)

    Map.put(attrs, :netmask, long_to_ip((0xffffffff <<< (32 - mask)) >>> 0))
    Map.put(attrs, :gateway, ip_to_long(first) &&& ((-1 <<< (32 - +mask))))

    ip_pool = insert(:ip_pool, attrs)

    ipv4 = ip_pool_list(first, mask)
    |> Enum.map(&insert(:ipv4, %{ip: &1, ip_pool: ip_pool}))

     Map.put(ip_pool, :ipv4, ipv4)
  end

  def ip_pool_factory() do
    %Webapp.Networks.IpPool{
      name: Faker.Internet.domain_word() <> " #{System.unique_integer()}",
      network: build(:network),
      gateway: Faker.Internet.ip_v4_address(),
      netmask: Faker.Internet.ip_v4_address(),
    }
  end

  def ipv4_factory() do
    %Webapp.Networks.Ipv4{
      ip: Faker.Internet.ip_v4_address(),
      ip_pool: build(:ip_pool)
    }
  end

  @doc """
  Generate a list of IP addresses from a given IP range.

  ## Examples

      iex> ip_pool_list("{192, 168, 0, 252}", 30)
      ["192.168.0.253", "192.168.0.254"]
  """
  def ip_pool_list(first, mask) do
    network_address = ip_to_long(first) &&& ((-1 <<< (32 - +mask)))
    broadcast_address = (network_address + (2 ** (32 - +mask)) - 1)

    long_first = network_address + 1
    long_last = broadcast_address - 1

    Enum.reduce(long_first..long_last, [], fn long, acc ->
      ip = long_to_ip(long)
      [ip | acc]
    end)
  end

  def ip_to_long({a, b, c, d}) do
   (a <<< 24 ||| b <<< 16 ||| c <<< 8 ||| d ) >>> 0
  end

  defp long_to_ip(long) do
    [
      (long &&& (0xff <<< 24)) >>> 24,
      (long &&& (0xff <<< 16)) >>> 16,
      (long &&& (0xff <<< 8)) >>> 8,
      long &&& 0xff
    ]
    |> Enum.join(".")
  end

  # {openssh_key, fingerprint}
  defp generate_ssh_public_key() do
    {:RSAPrivateKey, _, modulus, public_exponent, _, _, _, _exponent1, _, _, _otherPrimeInfos} =
      :public_key.generate_key({:rsa, 2048, 65_537})

    rsa_public_key = {:RSAPublicKey, modulus, public_exponent}

    {
      :ssh_file.encode([{rsa_public_key, [comment: ""]}], :openssh_key),
      to_string(:ssh.hostkey_fingerprint(rsa_public_key))
    }
  end

end
