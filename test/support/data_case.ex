defmodule Webapp.DataCase do
  @moduledoc """
  This module defines the setup for tests requiring
  access to the application's data layer.

  You may define functions here to be used as helpers in
  your tests.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      alias Webapp.Repo

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import Webapp.DataCase
      import Webapp.TestHelpers

      import Webapp.Factory
      import Webapp.AccountsFixtures
      import Webapp.DataHelpers, only: [datetime_now: 0]
    end
  end

  setup tags do
    Webapp.DataCase.setup_sandbox(tags)
    :ok
  end

  @doc """
  Sets up the sandbox based on the test tags.
  """
  def setup_sandbox(tags) do
    pid = Ecto.Adapters.SQL.Sandbox.start_owner!(Webapp.Repo, shared: not tags[:async])
    on_exit(fn -> Ecto.Adapters.SQL.Sandbox.stop_owner(pid) end)
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(%Ecto.Changeset{} = changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end

  # Transforms failed_value from the Ecto.Multi response
  def errors_on(failed_value) when is_map(failed_value) do
    Map.get(failed_value, :errors)
    |> Enum.map(fn {field, {message, _opts}} ->
      {field, [message]}
    end)
    |> Enum.into(%{})
  end

end
