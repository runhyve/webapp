defmodule WebappWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      alias WebappWeb.Router.Helpers, as: Routes

      import Webapp.TestHelpers
      import WebappWeb.ViewHelpers


      # The default endpoint for testing
      @endpoint WebappWeb.Endpoint

      import Webapp.DataHelpers, only: [datetime_now: 0]
      import Webapp.Factory
      import WebappWeb.ConnCase
      import Webapp.AccountsFixtures
      use WebappWeb, :verified_routes
    end
  end

  setup tags do
    Webapp.DataCase.setup_sandbox(tags)
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  @doc """
  Setup helper that registers and logs in users.

      setup :register_and_log_in_user

  It stores an updated connection and a registered user in the
  test context.
  """
  def register_and_log_in_user(%{conn: conn}) do
    user = Webapp.AccountsFixtures.user_fixture()
           |> Webapp.AccountsFixtures.confirm_user()

    user = Webapp.Repo.preload(user, :teams)
    team = hd(user.teams)

    %{conn: log_in_user(conn, user), user: user, team: team}
  end

  @doc """
  Setup helper that registers and logs in admin user.

      setup :register_and_log_in_admin

  It stores an updated connection and a registered admin user in the
  test context.
  """
  def register_and_log_in_admin(%{conn: conn}) do
    admin = Webapp.AccountsFixtures.admin_fixture()

    %{conn_admin: log_in_user(conn, admin), admin: admin}
  end



  @doc """
  Logs the given `user` into the `conn`.

  It returns an updated `conn`.
  """
  def log_in_user(conn, user) do
    token = Webapp.Accounts.generate_user_session_token(user)

    conn
    |> Phoenix.ConnTest.init_test_session(%{})
    |> Plug.Conn.put_session(:user_token, token)
  end
end
