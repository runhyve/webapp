defmodule Webapp.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Webapp.Accounts` context.
  """

  alias Webapp.Repo
  alias Webapp.Accounts


  def unique_user_name, do: "user#{System.unique_integer()}"
  def unique_user_email, do: "user#{System.unique_integer()}@example.com"
  def valid_user_password, do: "hello world!"
  def unique_team_name, do: "team#{System.unique_integer()}"

  def valid_user_attributes(attrs \\ %{}) do
    Enum.into(attrs, %{
      user_name: unique_user_name(),
      user_email: unique_user_email(),
      user_password: valid_user_password(),
      team_name: "Team #{unique_team_name()}",
      team_namespace: unique_team_name()
    })
  end

  def user_fixture(attrs \\ %{}) do
    {:ok, %{user: user}} =
      attrs
      |> valid_user_attributes()
      |> Webapp.Accounts.register_user()

      user
  end

  def admin_fixture(attrs \\ %{}) do
    user_fixture(attrs)
    |> Accounts.User.confirm_changeset()
    |> Ecto.Changeset.put_change(:role, "Administrator")
    |> Repo.update!()
  end

  def confirm_user(user) do
    Repo.update!(Accounts.User.confirm_changeset(user))
  end

  def force_password_update(user) do
    user
    |> Accounts.change_user_profile()
    |> Ecto.Changeset.put_change(:force_password_update, true)
    |> Repo.update!()
  end

  def extract_user_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end
end
