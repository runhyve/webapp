// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.

// BulmaJS
import Notification from '@vizuaalog/bulmajs/src/plugins/notification';
import Dropdown from '@vizuaalog/bulmajs/src/plugins/dropdown';

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

import slugify from './text/slugify'
slugify()

import navbarToggle from './components/navbar'
navbarToggle()

// Prevent Double Submits
document.querySelectorAll('form.phx-form-html').forEach(form => {
	form.addEventListener('submit', (e) => {
		if (form.classList.contains('is-submitting')) {
			e.preventDefault();
		}

		form.classList.add('is-submitting');

    form.querySelectorAll('button[phx-disable-with]').forEach(element => {
      element.disabled = true;
      element.innerHTML = element.getAttribute('phx-disable-with');
    });
	});
});