import { slugify as make_slug } from 'transliteration';

function slugify() {

  document.querySelectorAll('[data-slugify]').forEach((slug_field) => {
    let base_field = document.querySelector('[name="' + slug_field.dataset.slugify + '"]')
    let parent = slug_field.parentElement
    let value = base_field.value

    if (!parent.querySelector('span.slugify')) {
      let wrapper = document.createElement('div')
      wrapper.classList.add('flex', 'flex-row')
      let slug_text = document.createElement('span')
      slug_text.classList.add('slugify', 'text-gray-500', 'text-sm', 'grow')
      slug_text.textContent = make_slug(value)
      wrapper.appendChild(slug_text)

      slug_field.classList.add('is-hidden', 'hidden')

      let edit_button = document.createElement('span')
      edit_button.classList.add('edit-btn', 'hidden', 'inline-block', 'self-end', 'rounded', 'text-sm', 'underline', 'cursor-pointer')
      edit_button.textContent = 'Edit'
      edit_button.addEventListener('click', () => {
        slug_field.classList.remove('is-hidden', 'hidden')
        slug_text.remove()
        edit_button.remove()
        slug_field.focus()

        base_field.parentElement.dataset.manualSlug = true
        slug_field.parentElement.classList.remove('!mt-0')
        slug_field.parentElement.querySelector('label').classList.remove('hidden')
      })
      wrapper.appendChild(edit_button)
      parent.appendChild(wrapper)

      if (value !== '') {
        slug_field.parentElement.querySelector('span.edit-btn').classList.remove('hidden')
      }
    }

    ['keyup', 'change'].forEach((event_type) => {
      base_field.addEventListener(event_type, () => {
        if (base_field.parentElement.dataset.manualSlug) {
          return
        }

        let slug = make_slug(base_field.value)

        let slug_text = parent.querySelector('span.slugify')
        slug_text.textContent = slug
        slug_field.value = slug

        if (slug !== '') {
          slug_field.parentElement.querySelector('span.edit-btn').classList.remove('hidden')
        }
      })
    })
  })
}

export default slugify;